#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: reproductor.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

class Rhythmbox:
	name = "Rhythmbox"
	
	commands = {
			'abrir': 'no-start --play',
			'abre': 'no-start --play',
			'titulo': 'print-playing',
			'siguiente': 'next',
			'próximo': 'next',
			'cambiar': 'next',
			'anterior': 'previous',
			'mostrar': 'notify',
			'pausa': 'pause',
			'cerrar': 'quit',
			'cierra': 'quit',
			'bajar': 'volume-down',
			'subir': 'volume-up',
	}

	def parse(self, word):
		if word in self.commands:
			return 'rhythmbox-client --%s' % self.commands[word]

class Banshee:
	name = "Banshee"

	commands = {
			'abrir': 'play',
			'abre': 'play',
			'reproducir': 'play',
			'pausa': 'pause',
			'detener': 'stop',
			'siguiente': 'next',
			'anterior': 'previous',
			'silencio': 'pause',
			'cerrar': 'quit',
			'cierra': 'quit',
	}

	def parse(self, word):
		if word in self.commands:
			return 'banshee --no-present --%s %% ' % self.commands[word]
