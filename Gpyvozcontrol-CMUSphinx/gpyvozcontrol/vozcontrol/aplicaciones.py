#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: aplicaciones.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

from accion import Action
class Aplicaciones:
	name = "Aplicaciones"
	
	aplications = {
			'texto': 'gedit',
			'documentos': 'lowriter',
			'documento': 'lowriter',
			'calculo': 'localc',
			'hoja': 'localc',
			'calculadora': 'gnome-calculator',
			'wiki' : 'https://bitbucket.org/gproyectogrado/gpyvozcontrol/wiki/Home',
			'proyecto' : 'https://bitbucket.org/gproyectogrado/gpyvozcontrol',
			'mensajeria': 'pidgin',
			'terminal': 'gnome-terminal',
			'consola': 'gnome-terminal',
			'presentacion': 'loimpress',
			'navegador': 'http://www.google.com',
			'gmail' : 'http://gmail.com',
	}

	def parse(self, word):
		if word in self.aplications:
			return '%s &' % self.aplications[word]

class Accesibilidad:
	name = "Accesibilidad"

	assesibility = {
			'orca': 'orca',
			'teclado': 'florence',
			'magnificador': 'magnifier',
	}

	def parse(self, word):
		if word in self.assesibility:
			return '%s' % self.assesibility[word]

class Carpetas:
	ruta = Action()
	name = "Carpetas"

	folders = {
			'documentos': 'Documentos',
			'descargas': 'Descargas',
			'musica': 'Música',
			'videos': 'Vídeos',
			'personal': '',
	}

	def parse(self, word):
		if word in self.folders:
			return 'nautilus %s%s' % (ruta.home(), self.folders[word])
