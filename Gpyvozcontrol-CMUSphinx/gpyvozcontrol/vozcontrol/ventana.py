#!/usr/bin/python
# coding: utf-8
# action_ventana.py
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., Franklin Street, Fifth Floor,
# Boston MA  02110-1301 USA.
#
# Ejemplo de maximizar ventanas
# Presionando una tecla.

import pyatspi
import pygtk
import gtk
import wnck
import re
import sys
import time
from speak import Reproducir
from vozcontrol1 import CommandAndControl

screen = wnck.screen_get_default()
puente = CommandAndControl(sys.stdin)

# Callback to print the active window on key press amd filter out the key release
def input(event):
	params = puente.puente()
	if event.type == pyatspi.KEY_RELEASED_EVENT:
		return False
	if event.event_string=='F4':
		pyatspi.Registry.stop()
	elif params[0] == 'maximizar' or params[0] == 'maximiza':
		if params[1] == 'todas':
			Actions(Acciones(screen), 'maximize_all')
			return True
		else:
			Actions(Acciones(screen), 'maximizar')
			return True
	elif params[0] == 'minimizar' or params[0] == 'minimiza':
		if params[1] == 'todas':
			Actions(Acciones(screen), 'minimize_all')
			return True
		else:
			Actions(Acciones(screen), 'minimizar')
			return True
	elif params[0] == 'ampliar' or params[0] == 'amplia':
		if params[1] == 'todas':
			Actions(Acciones(screen), 'ampliar_all')
			return True
		else:
			Actions(Acciones(screen), 'ampliar')
			return True
	elif params[0] == 'reducir' or params[0] == 'reduce':
		if params[1] == 'todas':
			Actions(Acciones(screen), 'reduce_all')
			return True
		else:
			Actions(Acciones(screen), 'reducir')
			return True
	elif params[0] == 'cerrar' or params[0] == 'cierra':
		Actions(Acciones(screen), 'cerrar')
		return True

def active_window():
	desktop = pyatspi.Registry.getDesktop(0)
	for app in desktop:
		for window in app:
			if window.getState().contains(pyatspi.STATE_ACTIVE):
				return window

# Print hierarchy tree.
def Acciones(self, screen):
	while gtk.events_pending():
		gtk.main_iteration()
		windows = screen.get_windows()
		for w in windows:
			window = wnck.Screen.get_active_window(w.get_screen())
			return window, windows

def Actions(self, ventana, accion):
	if accion == 'maximizar':
		ventana[0].unminimize(0)
		print "Maximizando la ventana "+ventana[0].get_name()
		Reproducir("Se maximiza la ventana "+ventana[0].get_name())
	if accion == 'minimizar':
		ventana[0].minimize()
		print "Minimizando la ventana "+ventana[0].get_name()
		Reproducir("Se minimiza la ventana "+ventana[0].get_name())
	if accion == 'cerrar':
		ventana[0].close(int(time.time()))
		print 'Cerrando la ventana '+ventana[0].get_name()
		Reproducir('Cerrando la ventana '+ventana[0].get_name())
	if accion == 'minimize_all':
		print "Minimizando todas las ventanas"
		for w in ventana[1]:
			w.minimize()
		Reproducir("Se minimizan todas las ventanas")
	if accion == 'maximise_all':
		print "Maximizando todas las ventanas"
		for w in ventana[1]:
			w.unminimize(0)
		Reproducir("Se maximizan todas las ventanas")
	if accion == 'ampliar':
		ventana[0].maximize()
		print "Ampliando la ventana "+ventana[0].get_name()
		Reproducir("Se amplía la ventana "+ventana[0].get_name())
	if accion == 'reducir':
		ventana[0].unmaximize()
		print "Se reduce la ventana "+ventana[0].get_name()
	if accion == 'ampliar_all':
		print "Maximizando todas las ventanas"
		for w in ventana[1]:
			w.maximize(0)
		Reproducir('Se amplían todas las ventanas')
	if accion == 'reduce_all':
		print "Reduce todas las ventanas"
		for w in ventana[1]:
			w.unmaximize(0)
		Reproducir('Se Reducen todas las ventanas')

pyatspi.Registry.registerKeystrokeListener(on_key_input, kind=(pyatspi.KEY_PRESSED_EVENT, pyatspi.KEY_RELEASED_EVENT))
pyatspi.Registry.start()
