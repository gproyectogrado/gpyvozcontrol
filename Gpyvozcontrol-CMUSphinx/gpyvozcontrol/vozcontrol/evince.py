#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: evince.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import os
import re
from pyPdf import PdfFileReader
from speak import Reproducir
from accion import Action

class Evince:
	ruta = Action()
	file = PdfFileReader(file("%sProyecto.pdf" % ruta.home(), "rb"))
	paginas = file.getNumPages()

	def parse(self, word):
		pag = []
		if word == 'anterior':
			pag = self.nro_pag(1, 0)
		elif word == 'siguiente' or word == 'próximo' or word == 'cambiar':
			pag = self.nro_pag(0, 0)
		elif word == 'primera':
			pag = self.nro_pag(2, 0)
		elif word == 'ultima':
			pag = self.nro_pag(3, 0)
		elif re.findall('[0-9]+', word):
			pag = self.nro_pag(4, int(word))
		elif word == 'actual':
			pag = self.nro_pag(5, 0)
		else:
			pag = self.nro_pag(6, 0)

		return 'evince -s -f -i %s %sProyecto.pdf' % (str(pag[0]), ruta.home(), pag[1])

	def nro_pag(self, n, np):
		file = open('.pagina', 'r')
		pag = int(file.read())
		file2 = open('.paginatemp', 'w')
		t=0
		texto = ""

		if n == 1:
			if pag != 1:
				file2.write('%s' % str(pag-1))
				pag = pag - 1
			else:
				texto = 'Estoy en la primera lámina, no puedo ir a la anterior'
				file2.write('%s' % str(pag))
				t=1
		elif n == 0:
			if pag != paginas:
				file2.write('%s' % str(pag+1))
				pag = pag + 1
			else:
				texto = 'No puedo ir a la siguiente, no hay mas láminas'
				file2.write('%s' % str(pag))
				t=1
		elif n == 2:
			if pag != 1:
				file2.write('%s' % str(1))
				pag = 1
			else:
				file2.write('%s' % str(1))
				texto = 'Ya estoy en la primera lámina'
				t=1
		elif n == 3:
			if pag != paginas:
				file2.write('%s' % str(paginas))
				pag = paginas
			else:
				texto = 'Ya estoy en la última lámina'
				t=1
				file2.write('%s' % str(pag))
		elif n == 4:
			if pag != int(np) and int(np) <= paginas:
				file2.write('%s' % str(np))
				pag = int(np)
			elif int(np) > paginas:
				texto = 'No puedo ir a la lámina %s, solo hay %s láminas' % (int(np), paginas)
				t=1
				file2.write('%s' % str(pag))
			else:
				texto = 'Ya estoy en la lámina %s' % str(np)
				file2.write('%s' % str(pag))
				t=1
		elif n == 5:
			texto = "Estoy en la lámina %s" % int(pag)
			file2.write('%s' % str(pag))
			t=1
		else:
			texto = '¡Ups!, no entiendo la orden, repite de nuevo'
			file2.write('%s' % str(pag))
			t = 1

		if t == 0:
			texto = 'Cambiando a la lámina %s' % str(pag)
		file.close()
		file2.close()
		os.system('cat .paginatemp > .pagina')

		return int(pag), texto
