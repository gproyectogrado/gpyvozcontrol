#! /usr/bin/env python
# coding: utf-8

class Sistema():
	name = "Sistema"
	sistema = {
		'subir': 'amixer sset Master 5\%+',
		'bajar': 'amixer sset Master 5\%-',
		'apagar': 'halt',
		'reiniciar': 'reboot',
	}

	def parse(self, word):
		if word in self.sistema:
			return self.sistema[word]
