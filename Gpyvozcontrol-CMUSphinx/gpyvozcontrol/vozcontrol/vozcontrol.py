#!/usr/bin/env python
#coding: utf-8

from pocketsphinx import *
import pyaudio
import sys
import os
import time
from gpyvozcontrol import CommandAndControl
from speak import Reproducir

command = CommandAndControl()
hmm = '../model_parameters/gpyvozcontrol.cd_cont_200'
dic = '../etc/GpyVozControl.dic'
#lm= '../etc/voxforge_es_sphinx.transcription.test.lm'
gram = '../gpyvozcontrol.gram' 

os.system('amixer sset "Capture" 15800')
config = Decoder.default_config()
config.set_string('-hmm', hmm)
#config.set_string('-lm', lm)
config.set_string('-dict', dic)
config.set_string('-jsgf', gram)
config.set_string('-logfn', '/dev/null')

print 'GPYVOZCONTTROL - Sistema de órdenes de voz en español con PocketSphinx'
Reproducir('Bienvenido a GPYVOZCONTROL, el sistema de órdenes de voz en español con PocketSphinx')
Reproducir('Espera un momento a que se cargue el modelo acústico')
for i in range(1, 4):
	Reproducir('%s' % str(4-i))
	time.sleep(1)
Reproducir('Listopara recibir órdenes, ya puedes comenzar')

decoder = Decoder(config)

p = pyaudio.PyAudio()

stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=1024)
stream.start_stream()
in_speech_bf = True
decoder.start_utt()
while True:
    buf = stream.read(1024)
    if buf:
        decoder.process_raw(buf, False, False)
        #try:
            #if  decoder.hyp().hypstr != '':
                #print 'Partial decoding result:', decoder.hyp().hypstr
                #pass
        #except AttributeError:
            #pass
        if decoder.get_in_speech():
            sys.stdout.write('.')
            sys.stdout.flush()
        if decoder.get_in_speech() != in_speech_bf:
            in_speech_bf = decoder.get_in_speech()
            if not in_speech_bf:
                decoder.end_utt()
                try:
                    if  decoder.hyp().hypstr != '':
                        f = open('.orden', 'r')
                        k = int(f.read())
                        f.close()
                        print ' ', decoder.hyp().hypstr
                        if decoder.hyp().hypstr == 'vozcontrol' and k == 0 and decoder.hyp().hypstr != '':
                            os.system('echo 1 > .orden')
                            os.system('aplay .wavs/tono2.wav &')
                            print "Colocando 1 el archivo .orden"
                        if k == 1 and decoder.hyp().hypstr != '':
                            command.ordenes(decoder.hyp().hypstr)
                            os.system('echo 0 > .orden')
                            print "estoy dentro de la función command"
                except AttributeError:
                    pass
                decoder.start_utt()
    else:
        break
decoder.end_utt()
print 'An Error occured:', decoder.hyp().hypstr
