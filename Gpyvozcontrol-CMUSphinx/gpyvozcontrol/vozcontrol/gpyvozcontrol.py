#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: gpyvozcontrol.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import sys
import commands
import os
import re
import wnck
import time
import locale
import webbrowser
from multimedia import Multimedia
from aplicaciones import Aplicaciones, Accesibilidad, Carpetas
from evince import Evince
from speak import Reproducir
from sistema import Sistema
from clientecorreo import ClienteDeCorreo
from accion import Action
from numero import ConvertirNumero

class CommandAndControl:
	def ordenes(self, line):

		self.mediaplayer = Multimedia()
		self.applications = Aplicaciones()
		self.folders = Carpetas()
		self.actions = Action()
		self.evince = Evince()
		self.system = Sistema()
		screen = wnck.screen_get_default()
		print "la linea es ", line
		self.parse(line.strip('\n'), screen)

	def parse(self, line, screen):
			# Parse the input
			params = [param.lower() for param in line.split() if param]
			if not '-q' in sys.argv and not '--quiet' in sys.argv:
				print 'Entrada reconocida:', ' '. join(params).capitalize()
				Reproducir('Espere un momento')

			# Ejecuta el comando si es reconocido.
			#Reproducir('Espera un momento')

			if params[-1] == 'cancelar' or params[-1] == 'cancela':
				Reproducir('No se ejecuta ninguna orden')
			elif params[-1] == 'reproductor':
				command = self.mediaplayer.parse(params[0])
				if params[0] == 'abrir' or params[0] == 'abre':
					os.system(command)
					Reproducir('%s fue activado' % self.mediaplayer.name)
				elif command:
					if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
						os.system(command)
						Reproducir('%s %s a %s' % (params[0], params[1], self.mediaplayer.name))
					else:
						print'No tienes un reproductor abierto para realizar esa acción'
						Reproducir('No tienes un reproductor abierto para realizar esa acción')
				elif not '-q' in sys.argv and not '--quiet' in sys.argv:
					print 'Acción no válida para %s' % self.mediaplayer.name
					Reproducir('Acción no válida para %s' % self.mediaplayer.name)
			elif params[0] == 'cancion':
				command = self.mediaplayer.parse(params[-1])
				if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
					os.system(command)
					Reproducir('Reproduciendo la %s %s' % (params[-1], params[0]))
				else:
					print'No tienes un reproductor abierto'
					Reproducir('No tienes un reproductor abierto para realizar esa acción')
			elif params[-1] == 'cancion':
				command = self.mediaplayer.parse(params[0])
				if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
					if params[0] == 'próximo' or params[0] == 'cambiar':
						os.system(command)
						Reproducir('Se cambia la canción')
					else:
						print "No entiendo la orden"
						Reproducir("Ups! no entiendo la orden")
				else:
					print'No tienes un reproductor abierto'
					Reproducir('No tienes un reproductor abierto para realizar esa acción')
			elif params[0] == 'dime' or params[0] == 'decir':
				if params[-1] =='hora' or params[-1] == 'fecha':
					self.actions.sayDate(params[-1])
				else:
					self.actions.actions(self.actions.Acciones(screen), 'titulo', 2)
			elif params[0] == 'titulo':
				if params[-1] == 'cancion':
					command = self.mediaplayer.parse(params[0])
					if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
						titulo = commands.getoutput(command)
						Reproducir(str(titulo))
					else:
						print'No tienes un reproductor abierto'
						Reproducir('No tienes un reproductor abierto para realizar esa acción')
				else:
					self.actions.actions(self.actions.acciones(screen), 'titulo', 1)
			elif params[0] == 'abrir' or params[0] == 'abre':
				if params[1] == 'carpeta':
					command = self.folders.parse(params[-1])
					os.system(command)
					Reproducir('Carpeta %s fue abierta' % params[-1])
				elif params[-1] == 'correo':
					command = self.correo.parse(params[0])
					os.system(command)
					Reproducir('El cliente de correo %s fue abierto' % self.correo.name)
				else:
					command = self.applications.parse(params[-1])
					os.system(command)
					Reproducir('La aplicación %s fue abierta' % params[-1])
			elif params[-1] == 'sistema' or params[-1] == 'equipo':
				command = self.system.parse(params[0])
				os.system(command)
				if params[0] == 'apagar' or params[0] == 'reiniciar':
					Reproducir('El %s se va a %s' % (params[-1], params[0]))
				else:
					#texto = '%s %s al %s' % (params[0], params[1], params[-1])
					Reproducir('%s %s al %s' % (params[0], params[1], params[-1]))
			elif params[0] == 'presentar':
				os.system('evince -f -s /home/%s/Proyecto.pdf &' % self.actions.home())
				Reproducir('Proyecto de grado fue abierto')
			elif params[0] == 'lamina':
				command = self.evince.parse(params[-1])
				if os.system('ps -A | grep evince > /dev/null') == 0:
					os.system(command[0])
					Reproducir(command[1])
				else:
					print'No tienes un Evince abierto'
					Reproducir('No tienes un Evince abierto')
			elif params[-1] == 'lamina':
				command = self.evince.parse(params[0])
				if os.system('ps -A | grep evince > /dev/null') == 0:
					os.system(command[0])
					Reproducir(command[1])
				elif params[-1] == 'proyecto' or params[-1] == 'wiki' or params[-1] == 'navegador' or params[-1] == 'gmail':
					command = self.applications.parse(params[-1])
					webbrowser.open_new_tab(command)
					Reproducir('La página del %s fue abierta' % params[-1])
				else:
					print'No tienes un Evince abierto'
					Reproducir('No tienes un Evince abierto')
			elif params[0] == "cambiar" or params[0] == "cambia":
				self.actions.actions(self.actions.acciones(screen), params[0], 1)
				return True
			elif params[0] == 'maximizar' or params[0] == 'maximiza' or params[0] == 'minimizar' or params[0] == 'minimiza' or params[0] == 'reducir' or params[0] == 'reduce' or params[0] == 'ampliar' or params[0] == 'amplia' or params[0] == 'cerrar' or params[0] == 'cierra':
				if params[-1] != 'maximizar' and params[-1] != 'maximiza' and params[-1] != 'minimizar' and params[-1] != 'minimiza' and params[-1] != 'reducir' and params[-1] != 'reduce' and params[-1] != 'ampliar' and params[-1] != 'amplia' and params[-1] != 'cerrar' and params[-1] != 'cierra':
					if params[1] == 'todas' or params[1] == 'todo':
						self.actions.actions(self.actions.acciones(screen), params[0], 1)
						return True
					else:
						self.actions.actions(self.actions.acciones(screen), params[0], 2)
						return True
				else:
					self.actions.actions(self.actions.acciones(screen), params[0], 2)
					return True
			elif params[0] == 'cambiar' or params[0] == 'cambia':
				linea = ConvertirNumero(line)
				words = [param.lower() for param in linea.split() if param]
				command = self.evince.parse(words[-1])
				if os.system('ps -A | grep evince > /dev/null') == 0:
					os.system(command[0])
					Reproducir(command[1])
				else:
					print'No tienes un Evince abierto'
					Reproducir('No tienes un Evince abierto')
			else:
				print '¡Ups! No entiendo la orden'
				Reproducir('¡Ups! No entiendo la orden')
			os.system('echo 0 > .orden')
			screen = None

if __name__ == '__main__':
	try:
		CommandAndControl()
	except KeyboardInterrupt:
		sys.exit(1)
