#! /usr/bin/env python
# coding: utf-8

import os
import sys
from reproductor import  Rhythmbox,Banshee 
from speak import Reproducir

def Multimedia():
	mediaplayer=""
	if os.system('ps xa | grep -v grep | grep banshee >/dev/null') == 0:
		mediaplayer = Banshee()
	elif os.system('ps xa | grep -v grep | grep rhythmbox >/dev/null') == 0:
		mediaplayer = Rhythmbox()
	elif os.system('which banshee >/dev/null') == 0:
		mediaplayer = Banshee()
		os.system('bash -c "nohup banshee >/dev/null 2>&1 < &1 & disown %%"')
	elif os.system('which rhythmbox >/dev/null') == 0:
		mediaplayer = Rhythmbox()
	else:
		Reproducir('No se encuentra instalado ningun reproductor. ' \
				'Por favor debe instalar Rhythmbox o Banshee.')
		sys.exit(1)

	return mediaplayer
