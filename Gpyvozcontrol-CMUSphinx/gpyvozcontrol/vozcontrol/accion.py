#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: accion.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Julio - 2016
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import wnck
import pygtk
import gtk
import time
import locale
import commands
from speak import Reproducir

class Action:
  def acciones(self, screen):
    while gtk.events_pending():
      gtk.main_iteration()
      print "... force update ..."
      ventana = wnck.Screen.force_update(screen)
      print ventana 
      print "...end  force update ..."
      windows = screen.get_windows()
      for w in windows:
        return windows

  def actions(self, windows, accion, n):
    for w in windows:
      window = wnck.Screen.get_active_window(w.get_screen())

    if accion == 'maximizar' or accion == 'maximiza':
      if n == 2:
        win =  wnck.Screen.get_windows(wnck.screen_get_default())
        for w in win:
          w.unminimize(0)     
        #window.unminimize(0)
        print "Maximizando la ventana "+window.get_name()
        Reproducir("Se maximiza la ventana "+window.get_name())
      if n == 1:
        print "Maximizando todas las ventanas"
        win =  wnck.Screen.get_windows(wnck.screen_get_default())
        for w in win:
          w.unminimize(0)
        Reproducir("Se maximizan todas las ventanas")
    if accion == 'minimizar' or accion == 'minimiza':
      if n == 2:
        window.minimize()
        print "Minimizando la ventana "+window.get_name()
        Reproducir("Se minimiza la ventana "+window.get_name())
      if n == 1:
        print "Minimizando todas las ventanas"
        for w in windows:
          w.minimize()
        Reproducir("Se minimizan todas las ventanas")
    if accion == 'cerrar' or accion == 'cierra':
      if n == 2:
        window.close(int(time.time()))
        print 'Cerrando la ventana '+window.get_name()
        Reproducir('Cerrando la ventana '+window.get_name())
      if n == 1:
        win =  wnck.Screen.get_windows(wnck.screen_get_default())
        for w in win:
          w.close(int(time.time()))
        print "Se cierran todas las ventanas"
        Reproducir("Se cierran todas las ventanas")
    if accion == 'ampliar' or accion == 'amplia':
      if n == 2:
        window.maximize()
        print "Ampliando la ventana "+window.get_name()
        Reproducir("Se amplía la ventana "+window.get_name())
      if n == 1:
        print "Ampliando todas las ventanas"
        for w in windows:
          w.maximize(0)
        Reproducir('Se amplían todas las ventanas')
    if accion == 'reducir' or accion == 'reduce':
      if n == 2:
        window.unmaximize()
        print "Se reduce la ventana "+window.get_name()
        Reproducir("Se reduce la ventana "+window.get_name())
      if n == 1:
        print "Reduce todas las ventanas"
        for w in windows:
          w.unmaximize(0)
        Reproducir('Se Reducen todas las ventanas')
    if accion == 'titulo':
      if n == 1:
        win =  wnck.Screen.get_windows(wnck.screen_get_default())
        print "La ventana activa es "+window.get_name()
        Reproducir("La ventana activa es "+window.get_name())
      else:
        Reproducir("Las ventanas activas son:")
        for w in windows:
          Reproducir(" "+w.get_name())
    if accion == "cambiar" or accion == "cambia":
      t = 0
      k = 0
      for w in windows:
        if w.is_active():
          print "la ventana activa es "+w.get_name()
          t = k -1
          break;
        k += 1
      now = gtk.gdk.x11_get_server_time(gtk.gdk.get_default_root_window())
      windows[t].activate(now)
      windows[t].make_above()
      print "El valor de t es "+str(t)
      Reproducir("Se ha cambiado a la ventana "+windows[t].get_name())

  def sayDate(self, date):
    locale.setlocale(locale.LC_ALL, "")
    if date == 'fecha':
      fecha = time.strftime("%A , %d de %B del %Y", time.localtime())
      print fecha
      Reproducir(fecha)
    if date == 'hora':
      hora = time.strftime("son las %I y %M minutos")
      print hora
      Reproducir(hora)

  def home(self):
    personal =  commands.getoutput('whoami')
    return "/home/%s/" % personal

