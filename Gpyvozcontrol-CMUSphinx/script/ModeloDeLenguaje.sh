#! /bin/bash
# coding: utf-8
####################################################################
###
### archivo: ModeloDeLenguaje.sh
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

echo "Script para la creación  del modelo de lenguaje"
echo ""
echo "Eliminando el identificador del archivo gpyvozcontrol_train.transcription, se genera el archivo train.corpus y train.corpus"
python ../script/corpus.py

echo "Comienza el proceso de la creación del modelo"
cd lm/
cat train.corpus | ../../programas/CMU-Cam_Toolkit_v2/bin/text2wfreq | sort -rn -k 2 > train.wfreq
cat train.wfreq | ../../programas/CMU-Cam_Toolkit_v2/bin/wfreq2vocab -gt 0 > train.vocab
cat train.corpus | ../../programas/CMU-Cam_Toolkit_v2/bin/text2wngram -n 3 -temp /tmp >  train.w3gram
cat train.w3gram | ../../programas/CMU-Cam_Toolkit_v2/bin/wngram2idngram -n 3 -vocab train.vocab -temp /tmp > train.id3gram
../../programas/CMU-Cam_Toolkit_v2/bin/idngram2lm -idngram  train.id3gram -vocab train.vocab -context con.ccs -witten_bell -n 3 -vocab_type 1 -arpa train.lm

echo "Tranformando el modelo a formato de dump (DMP)"
../../programas/lm3g2dmp/lm3g2dmp train.lm ../etc/
cd ..
rm etc/gpyvozcontrol.lm.DMP
mv etc/train.lm.DMP etc/gpyvozcontrol.lm.DMP
