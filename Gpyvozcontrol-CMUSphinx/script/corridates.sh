#! /bin/bash
# coding: utf-8

echo "corridas para las pruebas de CMUSphinx"

echo "Corriendo la prueba 1"
cat etc/gpyvozcontrol_test1.fileids > etc/gpyvozcontrol_test.fileids
sphinxtrain -s decode run > prueba1.txt
cp -r result resultado1

echo "Corriendo la prueba 2"
cat etc/gpyvozcontrol_test2.fileids > etc/gpyvozcontrol_test.fileids
sphinxtrain -s decode run > prueba2.txt
cp -r result resultado2

echo "Corriendo la prueba 3"
cat etc/gpyvozcontrol_test3.fileids > etc/gpyvozcontrol_test.fileids
sphinxtrain -s decode run > prueba3.txt
cp -r result resultado3
