================= ============ ========================== ========================================================================================================
Code point(s)     Character(s) Tab completion sequence(s) Unicode name(s)
----------------- ------------ -------------------------- --------------------------------------------------------------------------------------------------------
U+000A1           ¡            \\textexclamdown           INVERTED EXCLAMATION MARK
U+000A3           £            \\sterling                 POUND SIGN
U+000A5           ¥            \\yen                      YEN SIGN
U+000A6           ¦            \\textbrokenbar            BROKEN BAR / BROKEN VERTICAL BAR
U+000A7           §            \\S                        SECTION SIGN
U+000A8           ¨            \\textasciidieresis        DIAERESIS / SPACING DIAERESIS
U+000A9           ©            \\copyright                COPYRIGHT SIGN
U+000AA           ª            \\textordfeminine          FEMININE ORDINAL INDICATOR
U+000AC           ¬            \\neg                      NOT SIGN
U+000AE           ®            \\circledR                 REGISTERED SIGN / REGISTERED TRADE MARK SIGN
U+000AF           ¯            \\textasciimacron          MACRON / SPACING MACRON
U+000B0           °            \\degree                   DEGREE SIGN
U+000B1           ±            \\pm                       PLUS-MINUS SIGN / PLUS-OR-MINUS SIGN
U+000B2           ²            \\^2                       SUPERSCRIPT TWO / SUPERSCRIPT DIGIT TWO
U+000B3           ³            \\^3                       SUPERSCRIPT THREE / SUPERSCRIPT DIGIT THREE
U+000B4           ´            \\textasciiacute           ACUTE ACCENT / SPACING ACUTE
U+000B6           ¶            \\P                        PILCROW SIGN / PARAGRAPH SIGN
U+000B7           ·            \\cdotp                    MIDDLE DOT
U+000B9           ¹            \\^1                       SUPERSCRIPT ONE / SUPERSCRIPT DIGIT ONE
U+000BA           º            \\textordmasculine         MASCULINE ORDINAL INDICATOR
U+000BC           ¼            \\textonequarter           VULGAR FRACTION ONE QUARTER / FRACTION ONE QUARTER
U+000BD           ½            \\textonehalf              VULGAR FRACTION ONE HALF / FRACTION ONE HALF
U+000BE           ¾            \\textthreequarters        VULGAR FRACTION THREE QUARTERS / FRACTION THREE QUARTERS
U+000BF           ¿            \\textquestiondown         INVERTED QUESTION MARK
U+000C5           Å            \\AA                       LATIN CAPITAL LETTER A WITH RING ABOVE / LATIN CAPITAL LETTER A RING
U+000C6           Æ            \\AE                       LATIN CAPITAL LETTER AE / LATIN CAPITAL LETTER A E
U+000D0           Ð            \\DH                       LATIN CAPITAL LETTER ETH
U+000D7           ×            \\times                    MULTIPLICATION SIGN
U+000D8           Ø            \\O                        LATIN CAPITAL LETTER O WITH STROKE / LATIN CAPITAL LETTER O SLASH
U+000DE           Þ            \\TH                       LATIN CAPITAL LETTER THORN
U+000DF           ß            \\ss                       LATIN SMALL LETTER SHARP S
U+000E5           å            \\aa                       LATIN SMALL LETTER A WITH RING ABOVE / LATIN SMALL LETTER A RING
U+000E6           æ            \\ae                       LATIN SMALL LETTER AE / LATIN SMALL LETTER A E
U+000F0           ð            \\eth                      LATIN SMALL LETTER ETH
U+000F7           ÷            \\div                      DIVISION SIGN
U+000F8           ø            \\o                        LATIN SMALL LETTER O WITH STROKE / LATIN SMALL LETTER O SLASH
U+000FE           þ            \\th                       LATIN SMALL LETTER THORN
U+00110           Đ            \\DJ                       LATIN CAPITAL LETTER D WITH STROKE / LATIN CAPITAL LETTER D BAR
U+00111           đ            \\dj                       LATIN SMALL LETTER D WITH STROKE / LATIN SMALL LETTER D BAR
U+00127           ħ            \\hbar, \\Elzxh            LATIN SMALL LETTER H WITH STROKE / LATIN SMALL LETTER H BAR
U+00131           ı            \\imath                    LATIN SMALL LETTER DOTLESS I
U+00141           Ł            \\L                        LATIN CAPITAL LETTER L WITH STROKE / LATIN CAPITAL LETTER L SLASH
U+00142           ł            \\l                        LATIN SMALL LETTER L WITH STROKE / LATIN SMALL LETTER L SLASH
U+0014A           Ŋ            \\NG                       LATIN CAPITAL LETTER ENG
U+0014B           ŋ            \\ng                       LATIN SMALL LETTER ENG
U+00152           Œ            \\OE                       LATIN CAPITAL LIGATURE OE / LATIN CAPITAL LETTER O E
U+00153           œ            \\oe                       LATIN SMALL LIGATURE OE / LATIN SMALL LETTER O E
U+00195           ƕ            \\texthvlig                LATIN SMALL LETTER HV / LATIN SMALL LETTER H V
U+0019E           ƞ            \\textnrleg                LATIN SMALL LETTER N WITH LONG RIGHT LEG
U+001C2           ǂ            \\textdoublepipe           LATIN LETTER ALVEOLAR CLICK / LATIN LETTER PIPE DOUBLE BAR
U+00250           ɐ            \\Elztrna                  LATIN SMALL LETTER TURNED A
U+00252           ɒ            \\Elztrnsa                 LATIN SMALL LETTER TURNED ALPHA / LATIN SMALL LETTER TURNED SCRIPT A
U+00254           ɔ            \\Elzopeno                 LATIN SMALL LETTER OPEN O
U+00256           ɖ            \\Elzrtld                  LATIN SMALL LETTER D WITH TAIL / LATIN SMALL LETTER D RETROFLEX HOOK
U+00259           ə            \\Elzschwa                 LATIN SMALL LETTER SCHWA
U+0025B           ɛ            \\varepsilon               LATIN SMALL LETTER OPEN E / LATIN SMALL LETTER EPSILON
U+00263           ɣ            \\Elzpgamma                LATIN SMALL LETTER GAMMA
U+00264           ɤ            \\Elzpbgam                 LATIN SMALL LETTER RAMS HORN / LATIN SMALL LETTER BABY GAMMA
U+00265           ɥ            \\Elztrnh                  LATIN SMALL LETTER TURNED H
U+0026C           ɬ            \\Elzbtdl                  LATIN SMALL LETTER L WITH BELT / LATIN SMALL LETTER L BELT
U+0026D           ɭ            \\Elzrtll                  LATIN SMALL LETTER L WITH RETROFLEX HOOK / LATIN SMALL LETTER L RETROFLEX HOOK
U+0026F           ɯ            \\Elztrnm                  LATIN SMALL LETTER TURNED M
U+00270           ɰ            \\Elztrnmlr                LATIN SMALL LETTER TURNED M WITH LONG LEG
U+00271           ɱ            \\Elzltlmr                 LATIN SMALL LETTER M WITH HOOK / LATIN SMALL LETTER M HOOK
U+00272           ɲ            \\Elzltln                  LATIN SMALL LETTER N WITH LEFT HOOK / LATIN SMALL LETTER N HOOK
U+00273           ɳ            \\Elzrtln                  LATIN SMALL LETTER N WITH RETROFLEX HOOK / LATIN SMALL LETTER N RETROFLEX HOOK
U+00277           ɷ            \\Elzclomeg                LATIN SMALL LETTER CLOSED OMEGA
U+00278           ɸ            \\textphi                  LATIN SMALL LETTER PHI
U+00279           ɹ            \\Elztrnr                  LATIN SMALL LETTER TURNED R
U+0027A           ɺ            \\Elztrnrl                 LATIN SMALL LETTER TURNED R WITH LONG LEG
U+0027B           ɻ            \\Elzrttrnr                LATIN SMALL LETTER TURNED R WITH HOOK / LATIN SMALL LETTER TURNED R HOOK
U+0027C           ɼ            \\Elzrl                    LATIN SMALL LETTER R WITH LONG LEG
U+0027D           ɽ            \\Elzrtlr                  LATIN SMALL LETTER R WITH TAIL / LATIN SMALL LETTER R HOOK
U+0027E           ɾ            \\Elzfhr                   LATIN SMALL LETTER R WITH FISHHOOK / LATIN SMALL LETTER FISHHOOK R
U+00282           ʂ            \\Elzrtls                  LATIN SMALL LETTER S WITH HOOK / LATIN SMALL LETTER S HOOK
U+00283           ʃ            \\Elzesh                   LATIN SMALL LETTER ESH
U+00287           ʇ            \\Elztrnt                  LATIN SMALL LETTER TURNED T
U+00288           ʈ            \\Elzrtlt                  LATIN SMALL LETTER T WITH RETROFLEX HOOK / LATIN SMALL LETTER T RETROFLEX HOOK
U+0028A           ʊ            \\Elzpupsil                LATIN SMALL LETTER UPSILON
U+0028B           ʋ            \\Elzpscrv                 LATIN SMALL LETTER V WITH HOOK / LATIN SMALL LETTER SCRIPT V
U+0028C           ʌ            \\Elzinvv                  LATIN SMALL LETTER TURNED V
U+0028D           ʍ            \\Elzinvw                  LATIN SMALL LETTER TURNED W
U+0028E           ʎ            \\Elztrny                  LATIN SMALL LETTER TURNED Y
U+00290           ʐ            \\Elzrtlz                  LATIN SMALL LETTER Z WITH RETROFLEX HOOK / LATIN SMALL LETTER Z RETROFLEX HOOK
U+00292           ʒ            \\Elzyogh                  LATIN SMALL LETTER EZH / LATIN SMALL LETTER YOGH
U+00294           ʔ            \\Elzglst                  LATIN LETTER GLOTTAL STOP
U+00295           ʕ            \\Elzreglst                LATIN LETTER PHARYNGEAL VOICED FRICATIVE / LATIN LETTER REVERSED GLOTTAL STOP
U+00296           ʖ            \\Elzinglst                LATIN LETTER INVERTED GLOTTAL STOP
U+0029E           ʞ            \\textturnk                LATIN SMALL LETTER TURNED K
U+002A4           ʤ            \\Elzdyogh                 LATIN SMALL LETTER DEZH DIGRAPH / LATIN SMALL LETTER D YOGH
U+002A7           ʧ            \\Elztesh                  LATIN SMALL LETTER TESH DIGRAPH / LATIN SMALL LETTER T ESH
U+002B0           ʰ            \\^h                       MODIFIER LETTER SMALL H
U+002B2           ʲ            \\^j                       MODIFIER LETTER SMALL J
U+002B3           ʳ            \\^r                       MODIFIER LETTER SMALL R
U+002B7           ʷ            \\^w                       MODIFIER LETTER SMALL W
U+002B8           ʸ            \\^y                       MODIFIER LETTER SMALL Y
U+002BC           ʼ            \\rasp                     MODIFIER LETTER APOSTROPHE
U+002C7           ˇ            \\textasciicaron           CARON / MODIFIER LETTER HACEK
U+002C8           ˈ            \\Elzverts                 MODIFIER LETTER VERTICAL LINE
U+002CC           ˌ            \\Elzverti                 MODIFIER LETTER LOW VERTICAL LINE
U+002D0           ː            \\Elzlmrk                  MODIFIER LETTER TRIANGULAR COLON
U+002D1           ˑ            \\Elzhlmrk                 MODIFIER LETTER HALF TRIANGULAR COLON
U+002D2           ˒            \\Elzsbrhr                 MODIFIER LETTER CENTRED RIGHT HALF RING / MODIFIER LETTER CENTERED RIGHT HALF RING
U+002D3           ˓            \\Elzsblhr                 MODIFIER LETTER CENTRED LEFT HALF RING / MODIFIER LETTER CENTERED LEFT HALF RING
U+002D4           ˔            \\Elzrais                  MODIFIER LETTER UP TACK
U+002D5           ˕            \\Elzlow                   MODIFIER LETTER DOWN TACK
U+002D8           ˘            \\u                        BREVE / SPACING BREVE
U+002DC           ˜            \\texttildelow             SMALL TILDE / SPACING TILDE
U+002E1           ˡ            \\^l                       MODIFIER LETTER SMALL L
U+002E2           ˢ            \\^s                       MODIFIER LETTER SMALL S
U+002E3           ˣ            \\^x                       MODIFIER LETTER SMALL X
U+00300           ̀             \\grave                    COMBINING GRAVE ACCENT / NON-SPACING GRAVE
U+00301           ́             \\acute                    COMBINING ACUTE ACCENT / NON-SPACING ACUTE
U+00302           ̂             \\hat                      COMBINING CIRCUMFLEX ACCENT / NON-SPACING CIRCUMFLEX
U+00303           ̃             \\tilde                    COMBINING TILDE / NON-SPACING TILDE
U+00304           ̄             \\bar                      COMBINING MACRON / NON-SPACING MACRON
U+00306           ̆             \\breve                    COMBINING BREVE / NON-SPACING BREVE
U+00307           ̇             \\dot                      COMBINING DOT ABOVE / NON-SPACING DOT ABOVE
U+00308           ̈             \\ddot                     COMBINING DIAERESIS / NON-SPACING DIAERESIS
U+0030A           ̊             \\ocirc                    COMBINING RING ABOVE / NON-SPACING RING ABOVE
U+0030B           ̋             \\H                        COMBINING DOUBLE ACUTE ACCENT / NON-SPACING DOUBLE ACUTE
U+0030C           ̌             \\check                    COMBINING CARON / NON-SPACING HACEK
U+00321           ̡             \\Elzpalh                  COMBINING PALATALIZED HOOK BELOW / NON-SPACING PALATALIZED HOOK BELOW
U+00322           ̢             \\Elzrh                    COMBINING RETROFLEX HOOK BELOW / NON-SPACING RETROFLEX HOOK BELOW
U+00327           ̧             \\c                        COMBINING CEDILLA / NON-SPACING CEDILLA
U+00328           ̨             \\k                        COMBINING OGONEK / NON-SPACING OGONEK
U+0032A           ̪             \\Elzsbbrg                 COMBINING BRIDGE BELOW / NON-SPACING BRIDGE BELOW
U+00335           ̵             \\Elzxl                    COMBINING SHORT STROKE OVERLAY / NON-SPACING SHORT BAR OVERLAY
U+00336           ̶             \\sout, \\Elzbar           COMBINING LONG STROKE OVERLAY / NON-SPACING LONG BAR OVERLAY
U+00391           Α            \\Alpha                    GREEK CAPITAL LETTER ALPHA
U+00392           Β            \\Beta                     GREEK CAPITAL LETTER BETA
U+00393           Γ            \\Gamma                    GREEK CAPITAL LETTER GAMMA
U+00394           Δ            \\Delta                    GREEK CAPITAL LETTER DELTA
U+00395           Ε            \\Epsilon                  GREEK CAPITAL LETTER EPSILON
U+00396           Ζ            \\Zeta                     GREEK CAPITAL LETTER ZETA
U+00397           Η            \\Eta                      GREEK CAPITAL LETTER ETA
U+00398           Θ            \\Theta                    GREEK CAPITAL LETTER THETA
U+00399           Ι            \\Iota                     GREEK CAPITAL LETTER IOTA
U+0039A           Κ            \\Kappa                    GREEK CAPITAL LETTER KAPPA
U+0039B           Λ            \\Lambda                   GREEK CAPITAL LETTER LAMDA / GREEK CAPITAL LETTER LAMBDA
U+0039E           Ξ            \\Xi                       GREEK CAPITAL LETTER XI
U+003A0           Π            \\Pi                       GREEK CAPITAL LETTER PI
U+003A1           Ρ            \\Rho                      GREEK CAPITAL LETTER RHO
U+003A3           Σ            \\Sigma                    GREEK CAPITAL LETTER SIGMA
U+003A4           Τ            \\Tau                      GREEK CAPITAL LETTER TAU
U+003A5           Υ            \\Upsilon                  GREEK CAPITAL LETTER UPSILON
U+003A6           Φ            \\Phi                      GREEK CAPITAL LETTER PHI
U+003A7           Χ            \\Chi                      GREEK CAPITAL LETTER CHI
U+003A8           Ψ            \\Psi                      GREEK CAPITAL LETTER PSI
U+003A9           Ω            \\Omega                    GREEK CAPITAL LETTER OMEGA
U+003B1           α            \\alpha                    GREEK SMALL LETTER ALPHA
U+003B2           β            \\beta                     GREEK SMALL LETTER BETA
U+003B3           γ            \\gamma                    GREEK SMALL LETTER GAMMA
U+003B4           δ            \\delta                    GREEK SMALL LETTER DELTA
U+003B6           ζ            \\zeta                     GREEK SMALL LETTER ZETA
U+003B7           η            \\eta                      GREEK SMALL LETTER ETA
U+003B8           θ            \\theta                    GREEK SMALL LETTER THETA
U+003B9           ι            \\iota                     GREEK SMALL LETTER IOTA
U+003BA           κ            \\kappa                    GREEK SMALL LETTER KAPPA
U+003BB           λ            \\lambda                   GREEK SMALL LETTER LAMDA / GREEK SMALL LETTER LAMBDA
U+003BC           μ            \\mu                       GREEK SMALL LETTER MU
U+003BD           ν            \\nu                       GREEK SMALL LETTER NU
U+003BE           ξ            \\xi                       GREEK SMALL LETTER XI
U+003C0           π            \\pi                       GREEK SMALL LETTER PI
U+003C1           ρ            \\rho                      GREEK SMALL LETTER RHO
U+003C2           ς            \\varsigma                 GREEK SMALL LETTER FINAL SIGMA
U+003C3           σ            \\sigma                    GREEK SMALL LETTER SIGMA
U+003C4           τ            \\tau                      GREEK SMALL LETTER TAU
U+003C5           υ            \\upsilon                  GREEK SMALL LETTER UPSILON
U+003C6           φ            \\varphi                   GREEK SMALL LETTER PHI
U+003C7           χ            \\chi                      GREEK SMALL LETTER CHI
U+003C8           ψ            \\psi                      GREEK SMALL LETTER PSI
U+003C9           ω            \\omega                    GREEK SMALL LETTER OMEGA
U+003D1           ϑ            \\vartheta                 GREEK THETA SYMBOL / GREEK SMALL LETTER SCRIPT THETA
U+003D5           ϕ            \\phi                      GREEK PHI SYMBOL / GREEK SMALL LETTER SCRIPT PHI
U+003D6           ϖ            \\varpi                    GREEK PI SYMBOL / GREEK SMALL LETTER OMEGA PI
U+003DA           Ϛ            \\Stigma                   GREEK LETTER STIGMA / GREEK CAPITAL LETTER STIGMA
U+003DC           Ϝ            \\Digamma                  GREEK LETTER DIGAMMA / GREEK CAPITAL LETTER DIGAMMA
U+003DD           ϝ            \\digamma                  GREEK SMALL LETTER DIGAMMA
U+003DE           Ϟ            \\Koppa                    GREEK LETTER KOPPA / GREEK CAPITAL LETTER KOPPA
U+003E0           Ϡ            \\Sampi                    GREEK LETTER SAMPI / GREEK CAPITAL LETTER SAMPI
U+003F0           ϰ            \\varkappa                 GREEK KAPPA SYMBOL / GREEK SMALL LETTER SCRIPT KAPPA
U+003F1           ϱ            \\varrho                   GREEK RHO SYMBOL / GREEK SMALL LETTER TAILED RHO
U+003F4           ϴ            \\textTheta                GREEK CAPITAL THETA SYMBOL
U+003F5           ϵ            \\epsilon                  GREEK LUNATE EPSILON SYMBOL
U+003F6           ϶            \\backepsilon              GREEK REVERSED LUNATE EPSILON SYMBOL
U+01D2C           ᴬ            \\^A                       MODIFIER LETTER CAPITAL A
U+01D2E           ᴮ            \\^B                       MODIFIER LETTER CAPITAL B
U+01D30           ᴰ            \\^D                       MODIFIER LETTER CAPITAL D
U+01D31           ᴱ            \\^E                       MODIFIER LETTER CAPITAL E
U+01D33           ᴳ            \\^G                       MODIFIER LETTER CAPITAL G
U+01D34           ᴴ            \\^H                       MODIFIER LETTER CAPITAL H
U+01D35           ᴵ            \\^I                       MODIFIER LETTER CAPITAL I
U+01D36           ᴶ            \\^J                       MODIFIER LETTER CAPITAL J
U+01D37           ᴷ            \\^K                       MODIFIER LETTER CAPITAL K
U+01D38           ᴸ            \\^L                       MODIFIER LETTER CAPITAL L
U+01D39           ᴹ            \\^M                       MODIFIER LETTER CAPITAL M
U+01D3A           ᴺ            \\^N                       MODIFIER LETTER CAPITAL N
U+01D3C           ᴼ            \\^O                       MODIFIER LETTER CAPITAL O
U+01D3E           ᴾ            \\^P                       MODIFIER LETTER CAPITAL P
U+01D3F           ᴿ            \\^R                       MODIFIER LETTER CAPITAL R
U+01D40           ᵀ            \\^T                       MODIFIER LETTER CAPITAL T
U+01D41           ᵁ            \\^U                       MODIFIER LETTER CAPITAL U
U+01D42           ᵂ            \\^W                       MODIFIER LETTER CAPITAL W
U+01D43           ᵃ            \\^a                       MODIFIER LETTER SMALL A
U+01D45           ᵅ            \\^alpha                   MODIFIER LETTER SMALL ALPHA
U+01D47           ᵇ            \\^b                       MODIFIER LETTER SMALL B
U+01D48           ᵈ            \\^d                       MODIFIER LETTER SMALL D
U+01D49           ᵉ            \\^e                       MODIFIER LETTER SMALL E
U+01D4B           ᵋ            \\^epsilon                 MODIFIER LETTER SMALL OPEN E
U+01D4D           ᵍ            \\^g                       MODIFIER LETTER SMALL G
U+01D4F           ᵏ            \\^k                       MODIFIER LETTER SMALL K
U+01D50           ᵐ            \\^m                       MODIFIER LETTER SMALL M
U+01D52           ᵒ            \\^o                       MODIFIER LETTER SMALL O
U+01D56           ᵖ            \\^p                       MODIFIER LETTER SMALL P
U+01D57           ᵗ            \\^t                       MODIFIER LETTER SMALL T
U+01D58           ᵘ            \\^u                       MODIFIER LETTER SMALL U
U+01D5B           ᵛ            \\^v                       MODIFIER LETTER SMALL V
U+01D5D           ᵝ            \\^beta                    MODIFIER LETTER SMALL BETA
U+01D5E           ᵞ            \\^gamma                   MODIFIER LETTER SMALL GREEK GAMMA
U+01D5F           ᵟ            \\^delta                   MODIFIER LETTER SMALL DELTA
U+01D60           ᵠ            \\^phi                     MODIFIER LETTER SMALL GREEK PHI
U+01D61           ᵡ            \\^chi                     MODIFIER LETTER SMALL CHI
U+01D62           ᵢ            \\_i                       LATIN SUBSCRIPT SMALL LETTER I
U+01D63           ᵣ            \\_r                       LATIN SUBSCRIPT SMALL LETTER R
U+01D64           ᵤ            \\_u                       LATIN SUBSCRIPT SMALL LETTER U
U+01D65           ᵥ            \\_v                       LATIN SUBSCRIPT SMALL LETTER V
U+01D66           ᵦ            \\_beta                    GREEK SUBSCRIPT SMALL LETTER BETA
U+01D67           ᵧ            \\_gamma                   GREEK SUBSCRIPT SMALL LETTER GAMMA
U+01D68           ᵨ            \\_rho                     GREEK SUBSCRIPT SMALL LETTER RHO
U+01D69           ᵩ            \\_phi                     GREEK SUBSCRIPT SMALL LETTER PHI
U+01D6A           ᵪ            \\_chi                     GREEK SUBSCRIPT SMALL LETTER CHI
U+01D9C           ᶜ            \\^c                       MODIFIER LETTER SMALL C
U+01DA0           ᶠ            \\^f                       MODIFIER LETTER SMALL F
U+01DA5           ᶥ            \\^iota                    MODIFIER LETTER SMALL IOTA
U+01DB2           ᶲ            \\^Phi                     MODIFIER LETTER SMALL PHI
U+01DBB           ᶻ            \\^z                       MODIFIER LETTER SMALL Z
U+01DBF           ᶿ            \\^theta                   MODIFIER LETTER SMALL THETA
U+02002                        \\enspace                  EN SPACE
U+02003                        \\quad                     EM SPACE
U+02005                        \\thickspace               FOUR-PER-EM SPACE
U+02009                        \\thinspace                THIN SPACE
U+0200A                        \\hspace                   HAIR SPACE
U+02013           –            \\endash                   EN DASH
U+02014           —            \\emdash                   EM DASH
U+02016           ‖            \\Vert                     DOUBLE VERTICAL LINE / DOUBLE VERTICAL BAR
U+02018           ‘            \\lq                       LEFT SINGLE QUOTATION MARK / SINGLE TURNED COMMA QUOTATION MARK
U+02019           ’            \\rq                       RIGHT SINGLE QUOTATION MARK / SINGLE COMMA QUOTATION MARK
U+0201B           ‛            \\Elzreapos                SINGLE HIGH-REVERSED-9 QUOTATION MARK / SINGLE REVERSED COMMA QUOTATION MARK
U+0201C           “            \\textquotedblleft         LEFT DOUBLE QUOTATION MARK / DOUBLE TURNED COMMA QUOTATION MARK
U+0201D           ”            \\textquotedblright        RIGHT DOUBLE QUOTATION MARK / DOUBLE COMMA QUOTATION MARK
U+02020           †            \\dagger                   DAGGER
U+02021           ‡            \\ddagger                  DOUBLE DAGGER
U+02022           •            \\bullet                   BULLET
U+02026           …            \\dots                     HORIZONTAL ELLIPSIS
U+02030           ‰            \\textperthousand          PER MILLE SIGN
U+02031           ‱            \\textpertenthousand       PER TEN THOUSAND SIGN
U+02032           ′            \\prime                    PRIME
U+02035           ‵            \\backprime                REVERSED PRIME
U+02039           ‹            \\guilsinglleft            SINGLE LEFT-POINTING ANGLE QUOTATION MARK / LEFT POINTING SINGLE GUILLEMET
U+0203A           ›            \\guilsinglright           SINGLE RIGHT-POINTING ANGLE QUOTATION MARK / RIGHT POINTING SINGLE GUILLEMET
U+02060           ⁠             \\nolinebreak              WORD JOINER
U+02070           ⁰            \\^0                       SUPERSCRIPT ZERO / SUPERSCRIPT DIGIT ZERO
U+02071           ⁱ            \\^i                       SUPERSCRIPT LATIN SMALL LETTER I
U+02074           ⁴            \\^4                       SUPERSCRIPT FOUR / SUPERSCRIPT DIGIT FOUR
U+02075           ⁵            \\^5                       SUPERSCRIPT FIVE / SUPERSCRIPT DIGIT FIVE
U+02076           ⁶            \\^6                       SUPERSCRIPT SIX / SUPERSCRIPT DIGIT SIX
U+02077           ⁷            \\^7                       SUPERSCRIPT SEVEN / SUPERSCRIPT DIGIT SEVEN
U+02078           ⁸            \\^8                       SUPERSCRIPT EIGHT / SUPERSCRIPT DIGIT EIGHT
U+02079           ⁹            \\^9                       SUPERSCRIPT NINE / SUPERSCRIPT DIGIT NINE
U+0207A           ⁺            \\^+                       SUPERSCRIPT PLUS SIGN
U+0207B           ⁻            \\^-                       SUPERSCRIPT MINUS / SUPERSCRIPT HYPHEN-MINUS
U+0207C           ⁼            \\^=                       SUPERSCRIPT EQUALS SIGN
U+0207D           ⁽            \\^(                       SUPERSCRIPT LEFT PARENTHESIS / SUPERSCRIPT OPENING PARENTHESIS
U+0207E           ⁾            \\^)                       SUPERSCRIPT RIGHT PARENTHESIS / SUPERSCRIPT CLOSING PARENTHESIS
U+0207F           ⁿ            \\^n                       SUPERSCRIPT LATIN SMALL LETTER N
U+02080           ₀            \\_0                       SUBSCRIPT ZERO / SUBSCRIPT DIGIT ZERO
U+02081           ₁            \\_1                       SUBSCRIPT ONE / SUBSCRIPT DIGIT ONE
U+02082           ₂            \\_2                       SUBSCRIPT TWO / SUBSCRIPT DIGIT TWO
U+02083           ₃            \\_3                       SUBSCRIPT THREE / SUBSCRIPT DIGIT THREE
U+02084           ₄            \\_4                       SUBSCRIPT FOUR / SUBSCRIPT DIGIT FOUR
U+02085           ₅            \\_5                       SUBSCRIPT FIVE / SUBSCRIPT DIGIT FIVE
U+02086           ₆            \\_6                       SUBSCRIPT SIX / SUBSCRIPT DIGIT SIX
U+02087           ₇            \\_7                       SUBSCRIPT SEVEN / SUBSCRIPT DIGIT SEVEN
U+02088           ₈            \\_8                       SUBSCRIPT EIGHT / SUBSCRIPT DIGIT EIGHT
U+02089           ₉            \\_9                       SUBSCRIPT NINE / SUBSCRIPT DIGIT NINE
U+0208A           ₊            \\_+                       SUBSCRIPT PLUS SIGN
U+0208B           ₋            \\_-                       SUBSCRIPT MINUS / SUBSCRIPT HYPHEN-MINUS
U+0208C           ₌            \\_=                       SUBSCRIPT EQUALS SIGN
U+0208D           ₍            \\_(                       SUBSCRIPT LEFT PARENTHESIS / SUBSCRIPT OPENING PARENTHESIS
U+0208E           ₎            \\_)                       SUBSCRIPT RIGHT PARENTHESIS / SUBSCRIPT CLOSING PARENTHESIS
U+02090           ₐ            \\_a                       LATIN SUBSCRIPT SMALL LETTER A
U+02091           ₑ            \\_e                       LATIN SUBSCRIPT SMALL LETTER E
U+02092           ₒ            \\_o                       LATIN SUBSCRIPT SMALL LETTER O
U+02093           ₓ            \\_x                       LATIN SUBSCRIPT SMALL LETTER X
U+02094           ₔ            \\_schwa                   LATIN SUBSCRIPT SMALL LETTER SCHWA
U+02095           ₕ            \\_h                       LATIN SUBSCRIPT SMALL LETTER H
U+02096           ₖ            \\_k                       LATIN SUBSCRIPT SMALL LETTER K
U+02097           ₗ            \\_l                       LATIN SUBSCRIPT SMALL LETTER L
U+02098           ₘ            \\_m                       LATIN SUBSCRIPT SMALL LETTER M
U+02099           ₙ            \\_n                       LATIN SUBSCRIPT SMALL LETTER N
U+0209A           ₚ            \\_p                       LATIN SUBSCRIPT SMALL LETTER P
U+0209B           ₛ            \\_s                       LATIN SUBSCRIPT SMALL LETTER S
U+0209C           ₜ            \\_t                       LATIN SUBSCRIPT SMALL LETTER T
U+020A7           ₧            \\Elzpes                   PESETA SIGN
U+020AC           €            \\euro                     EURO SIGN
U+020DB           ⃛             \\dddot                    COMBINING THREE DOTS ABOVE / NON-SPACING THREE DOTS ABOVE
U+020DC           ⃜             \\ddddot                   COMBINING FOUR DOTS ABOVE / NON-SPACING FOUR DOTS ABOVE
U+0210F           ℏ            \\hslash                   PLANCK CONSTANT OVER TWO PI / PLANCK CONSTANT OVER 2 PI
U+02111           ℑ            \\Im                       BLACK-LETTER CAPITAL I / BLACK-LETTER I
U+02113           ℓ            \\ell                      SCRIPT SMALL L
U+02116           №            \\textnumero               NUMERO SIGN / NUMERO
U+02118           ℘            \\wp                       SCRIPT CAPITAL P / SCRIPT P
U+0211C           ℜ            \\Re                       BLACK-LETTER CAPITAL R / BLACK-LETTER R
U+0211E           ℞            \\Elzxrat                  PRESCRIPTION TAKE
U+02122           ™            \\texttrademark            TRADE MARK SIGN / TRADEMARK
U+02127           ℧            \\mho                      INVERTED OHM SIGN / MHO
U+02135           ℵ            \\aleph                    ALEF SYMBOL / FIRST TRANSFINITE CARDINAL
U+02136           ℶ            \\beth                     BET SYMBOL / SECOND TRANSFINITE CARDINAL
U+02137           ℷ            \\gimel                    GIMEL SYMBOL / THIRD TRANSFINITE CARDINAL
U+02138           ℸ            \\daleth                   DALET SYMBOL / FOURTH TRANSFINITE CARDINAL
U+0213F           ℿ            \\BbbPi                    DOUBLE-STRUCK CAPITAL PI
U+02140           ⅀            \\bbsum                    DOUBLE-STRUCK N-ARY SUMMATION
U+02141           ⅁            \\Game                     TURNED SANS-SERIF CAPITAL G
U+02190           ←            \\leftarrow                LEFTWARDS ARROW / LEFT ARROW
U+02191           ↑            \\uparrow                  UPWARDS ARROW / UP ARROW
U+02192           →            \\rightarrow               RIGHTWARDS ARROW / RIGHT ARROW
U+02193           ↓            \\downarrow                DOWNWARDS ARROW / DOWN ARROW
U+02194           ↔            \\leftrightarrow           LEFT RIGHT ARROW
U+02195           ↕            \\updownarrow              UP DOWN ARROW
U+02196           ↖            \\nwarrow                  NORTH WEST ARROW / UPPER LEFT ARROW
U+02197           ↗            \\nearrow                  NORTH EAST ARROW / UPPER RIGHT ARROW
U+02198           ↘            \\searrow                  SOUTH EAST ARROW / LOWER RIGHT ARROW
U+02199           ↙            \\swarrow                  SOUTH WEST ARROW / LOWER LEFT ARROW
U+0219A           ↚            \\nleftarrow               LEFTWARDS ARROW WITH STROKE / LEFT ARROW WITH STROKE
U+0219B           ↛            \\nrightarrow              RIGHTWARDS ARROW WITH STROKE / RIGHT ARROW WITH STROKE
U+0219C           ↜            \\leftsquigarrow           LEFTWARDS WAVE ARROW / LEFT WAVE ARROW
U+0219D           ↝            \\rightsquigarrow          RIGHTWARDS WAVE ARROW / RIGHT WAVE ARROW
U+0219E           ↞            \\twoheadleftarrow         LEFTWARDS TWO HEADED ARROW / LEFT TWO HEADED ARROW
U+021A0           ↠            \\twoheadrightarrow        RIGHTWARDS TWO HEADED ARROW / RIGHT TWO HEADED ARROW
U+021A2           ↢            \\leftarrowtail            LEFTWARDS ARROW WITH TAIL / LEFT ARROW WITH TAIL
U+021A3           ↣            \\rightarrowtail           RIGHTWARDS ARROW WITH TAIL / RIGHT ARROW WITH TAIL
U+021A6           ↦            \\mapsto                   RIGHTWARDS ARROW FROM BAR / RIGHT ARROW FROM BAR
U+021A9           ↩            \\hookleftarrow            LEFTWARDS ARROW WITH HOOK / LEFT ARROW WITH HOOK
U+021AA           ↪            \\hookrightarrow           RIGHTWARDS ARROW WITH HOOK / RIGHT ARROW WITH HOOK
U+021AB           ↫            \\looparrowleft            LEFTWARDS ARROW WITH LOOP / LEFT ARROW WITH LOOP
U+021AC           ↬            \\looparrowright           RIGHTWARDS ARROW WITH LOOP / RIGHT ARROW WITH LOOP
U+021AD           ↭            \\leftrightsquigarrow      LEFT RIGHT WAVE ARROW
U+021AE           ↮            \\nleftrightarrow          LEFT RIGHT ARROW WITH STROKE
U+021B0           ↰            \\Lsh                      UPWARDS ARROW WITH TIP LEFTWARDS / UP ARROW WITH TIP LEFT
U+021B1           ↱            \\Rsh                      UPWARDS ARROW WITH TIP RIGHTWARDS / UP ARROW WITH TIP RIGHT
U+021B6           ↶            \\curvearrowleft           ANTICLOCKWISE TOP SEMICIRCLE ARROW
U+021B7           ↷            \\curvearrowright          CLOCKWISE TOP SEMICIRCLE ARROW
U+021BA           ↺            \\circlearrowleft          ANTICLOCKWISE OPEN CIRCLE ARROW
U+021BB           ↻            \\circlearrowright         CLOCKWISE OPEN CIRCLE ARROW
U+021BC           ↼            \\leftharpoonup            LEFTWARDS HARPOON WITH BARB UPWARDS / LEFT HARPOON WITH BARB UP
U+021BD           ↽            \\leftharpoondown          LEFTWARDS HARPOON WITH BARB DOWNWARDS / LEFT HARPOON WITH BARB DOWN
U+021BE           ↾            \\upharpoonleft            UPWARDS HARPOON WITH BARB RIGHTWARDS / UP HARPOON WITH BARB RIGHT
U+021BF           ↿            \\upharpoonright           UPWARDS HARPOON WITH BARB LEFTWARDS / UP HARPOON WITH BARB LEFT
U+021C0           ⇀            \\rightharpoonup           RIGHTWARDS HARPOON WITH BARB UPWARDS / RIGHT HARPOON WITH BARB UP
U+021C1           ⇁            \\rightharpoondown         RIGHTWARDS HARPOON WITH BARB DOWNWARDS / RIGHT HARPOON WITH BARB DOWN
U+021C2           ⇂            \\downharpoonright         DOWNWARDS HARPOON WITH BARB RIGHTWARDS / DOWN HARPOON WITH BARB RIGHT
U+021C3           ⇃            \\downharpoonleft          DOWNWARDS HARPOON WITH BARB LEFTWARDS / DOWN HARPOON WITH BARB LEFT
U+021C4           ⇄            \\rightleftarrows          RIGHTWARDS ARROW OVER LEFTWARDS ARROW / RIGHT ARROW OVER LEFT ARROW
U+021C5           ⇅            \\dblarrowupdown           UPWARDS ARROW LEFTWARDS OF DOWNWARDS ARROW / UP ARROW LEFT OF DOWN ARROW
U+021C6           ⇆            \\leftrightarrows          LEFTWARDS ARROW OVER RIGHTWARDS ARROW / LEFT ARROW OVER RIGHT ARROW
U+021C7           ⇇            \\leftleftarrows           LEFTWARDS PAIRED ARROWS / LEFT PAIRED ARROWS
U+021C8           ⇈            \\upuparrows               UPWARDS PAIRED ARROWS / UP PAIRED ARROWS
U+021C9           ⇉            \\rightrightarrows         RIGHTWARDS PAIRED ARROWS / RIGHT PAIRED ARROWS
U+021CA           ⇊            \\downdownarrows           DOWNWARDS PAIRED ARROWS / DOWN PAIRED ARROWS
U+021CB           ⇋            \\leftrightharpoons        LEFTWARDS HARPOON OVER RIGHTWARDS HARPOON / LEFT HARPOON OVER RIGHT HARPOON
U+021CC           ⇌            \\rightleftharpoons        RIGHTWARDS HARPOON OVER LEFTWARDS HARPOON / RIGHT HARPOON OVER LEFT HARPOON
U+021CD           ⇍            \\nLeftarrow               LEFTWARDS DOUBLE ARROW WITH STROKE / LEFT DOUBLE ARROW WITH STROKE
U+021CF           ⇏            \\nRightarrow              RIGHTWARDS DOUBLE ARROW WITH STROKE / RIGHT DOUBLE ARROW WITH STROKE
U+021D0           ⇐            \\Leftarrow                LEFTWARDS DOUBLE ARROW / LEFT DOUBLE ARROW
U+021D1           ⇑            \\Uparrow                  UPWARDS DOUBLE ARROW / UP DOUBLE ARROW
U+021D2           ⇒            \\Rightarrow               RIGHTWARDS DOUBLE ARROW / RIGHT DOUBLE ARROW
U+021D3           ⇓            \\Downarrow                DOWNWARDS DOUBLE ARROW / DOWN DOUBLE ARROW
U+021D4           ⇔            \\Leftrightarrow           LEFT RIGHT DOUBLE ARROW
U+021D5           ⇕            \\Updownarrow              UP DOWN DOUBLE ARROW
U+021DA           ⇚            \\Lleftarrow               LEFTWARDS TRIPLE ARROW / LEFT TRIPLE ARROW
U+021DB           ⇛            \\Rrightarrow              RIGHTWARDS TRIPLE ARROW / RIGHT TRIPLE ARROW
U+021F5           ⇵            \\DownArrowUpArrow         DOWNWARDS ARROW LEFTWARDS OF UPWARDS ARROW
U+021FD           ⇽            \\leftarrowtriangle        LEFTWARDS OPEN-HEADED ARROW
U+021FE           ⇾            \\rightarrowtriangle       RIGHTWARDS OPEN-HEADED ARROW
U+02200           ∀            \\forall                   FOR ALL
U+02201           ∁            \\complement               COMPLEMENT
U+02202           ∂            \\partial                  PARTIAL DIFFERENTIAL
U+02203           ∃            \\exists                   THERE EXISTS
U+02204           ∄            \\nexists                  THERE DOES NOT EXIST
U+02205           ∅            \\varnothing               EMPTY SET
U+02207           ∇            \\del, \\nabla             NABLA
U+02208           ∈            \\in                       ELEMENT OF
U+02209           ∉            \\notin                    NOT AN ELEMENT OF
U+0220B           ∋            \\ni                       CONTAINS AS MEMBER
U+0220F           ∏            \\prod                     N-ARY PRODUCT
U+02210           ∐            \\coprod                   N-ARY COPRODUCT
U+02211           ∑            \\sum                      N-ARY SUMMATION
U+02212           −            \\minus                    MINUS SIGN
U+02213           ∓            \\mp                       MINUS-OR-PLUS SIGN
U+02214           ∔            \\dotplus                  DOT PLUS
U+02216           ∖            \\setminus                 SET MINUS
U+02217           ∗            \\ast                      ASTERISK OPERATOR
U+02218           ∘            \\circ                     RING OPERATOR
U+0221A           √            \\surd, \\sqrt             SQUARE ROOT
U+0221B           ∛            \\cbrt                     CUBE ROOT
U+0221D           ∝            \\propto                   PROPORTIONAL TO
U+0221E           ∞            \\infty                    INFINITY
U+0221F           ∟            \\rightangle               RIGHT ANGLE
U+02220           ∠            \\angle                    ANGLE
U+02221           ∡            \\measuredangle            MEASURED ANGLE
U+02222           ∢            \\sphericalangle           SPHERICAL ANGLE
U+02223           ∣            \\mid                      DIVIDES
U+02224           ∤            \\nmid                     DOES NOT DIVIDE
U+02225           ∥            \\parallel                 PARALLEL TO
U+02226           ∦            \\nparallel                NOT PARALLEL TO
U+02227           ∧            \\wedge                    LOGICAL AND
U+02228           ∨            \\vee                      LOGICAL OR
U+02229           ∩            \\cap                      INTERSECTION
U+0222A           ∪            \\cup                      UNION
U+0222B           ∫            \\int                      INTEGRAL
U+0222C           ∬            \\iint                     DOUBLE INTEGRAL
U+0222D           ∭            \\iiint                    TRIPLE INTEGRAL
U+0222E           ∮            \\oint                     CONTOUR INTEGRAL
U+0222F           ∯            \\oiint                    SURFACE INTEGRAL
U+02230           ∰            \\oiiint                   VOLUME INTEGRAL
U+02231           ∱            \\clwintegral              CLOCKWISE INTEGRAL
U+02234           ∴            \\therefore                THEREFORE
U+02235           ∵            \\because                  BECAUSE
U+02237           ∷            \\Colon                    PROPORTION
U+02238           ∸            \\dotminus                 DOT MINUS
U+0223B           ∻            \\kernelcontraction        HOMOTHETIC
U+0223C           ∼            \\sim                      TILDE OPERATOR
U+0223D           ∽            \\backsim                  REVERSED TILDE
U+0223E           ∾            \\lazysinv                 INVERTED LAZY S
U+02240           ≀            \\wr                       WREATH PRODUCT
U+02241           ≁            \\nsim                     NOT TILDE
U+02242           ≂            \\eqsim                    MINUS TILDE
U+02242 + U+00338 ≂̸            \\neqsim                   MINUS TILDE + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02243           ≃            \\simeq                    ASYMPTOTICALLY EQUAL TO
U+02244           ≄            \\nsime                    NOT ASYMPTOTICALLY EQUAL TO
U+02245           ≅            \\cong                     APPROXIMATELY EQUAL TO
U+02246           ≆            \\approxnotequal           APPROXIMATELY BUT NOT ACTUALLY EQUAL TO
U+02247           ≇            \\ncong                    NEITHER APPROXIMATELY NOR ACTUALLY EQUAL TO
U+02248           ≈            \\approx                   ALMOST EQUAL TO
U+02249           ≉            \\napprox                  NOT ALMOST EQUAL TO
U+0224A           ≊            \\approxeq                 ALMOST EQUAL OR EQUAL TO
U+0224B           ≋            \\tildetrpl                TRIPLE TILDE
U+0224C           ≌            \\allequal                 ALL EQUAL TO
U+0224D           ≍            \\asymp                    EQUIVALENT TO
U+0224E           ≎            \\Bumpeq                   GEOMETRICALLY EQUIVALENT TO
U+0224E + U+00338 ≎̸            \\nBumpeq                  GEOMETRICALLY EQUIVALENT TO + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+0224F           ≏            \\bumpeq                   DIFFERENCE BETWEEN
U+0224F + U+00338 ≏̸            \\nbumpeq                  DIFFERENCE BETWEEN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02250           ≐            \\doteq                    APPROACHES THE LIMIT
U+02251           ≑            \\Doteq                    GEOMETRICALLY EQUAL TO
U+02252           ≒            \\fallingdotseq            APPROXIMATELY EQUAL TO OR THE IMAGE OF
U+02253           ≓            \\risingdotseq             IMAGE OF OR APPROXIMATELY EQUAL TO
U+02254           ≔            \\coloneq                  COLON EQUALS / COLON EQUAL
U+02255           ≕            \\eqcolon                  EQUALS COLON / EQUAL COLON
U+02256           ≖            \\eqcirc                   RING IN EQUAL TO
U+02257           ≗            \\circeq                   RING EQUAL TO
U+02259           ≙            \\wedgeq                   ESTIMATES
U+0225B           ≛            \\starequal                STAR EQUALS
U+0225C           ≜            \\triangleq                DELTA EQUAL TO
U+0225F           ≟            \\questeq                  QUESTIONED EQUAL TO
U+02260           ≠            \\ne                       NOT EQUAL TO
U+02261           ≡            \\equiv                    IDENTICAL TO
U+02262           ≢            \\nequiv                   NOT IDENTICAL TO
U+02264           ≤            \\le                       LESS-THAN OR EQUAL TO / LESS THAN OR EQUAL TO
U+02265           ≥            \\ge                       GREATER-THAN OR EQUAL TO / GREATER THAN OR EQUAL TO
U+02266           ≦            \\leqq                     LESS-THAN OVER EQUAL TO / LESS THAN OVER EQUAL TO
U+02267           ≧            \\geqq                     GREATER-THAN OVER EQUAL TO / GREATER THAN OVER EQUAL TO
U+02268           ≨            \\lneqq                    LESS-THAN BUT NOT EQUAL TO / LESS THAN BUT NOT EQUAL TO
U+02268 + U+0FE00 ≨︀            \\lvertneqq                LESS-THAN BUT NOT EQUAL TO / LESS THAN BUT NOT EQUAL TO + VARIATION SELECTOR-1
U+02269           ≩            \\gneqq                    GREATER-THAN BUT NOT EQUAL TO / GREATER THAN BUT NOT EQUAL TO
U+02269 + U+0FE00 ≩︀            \\gvertneqq                GREATER-THAN BUT NOT EQUAL TO / GREATER THAN BUT NOT EQUAL TO + VARIATION SELECTOR-1
U+0226A           ≪            \\ll                       MUCH LESS-THAN / MUCH LESS THAN
U+0226A + U+00338 ≪̸            \\NotLessLess              MUCH LESS-THAN / MUCH LESS THAN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+0226B           ≫            \\gg                       MUCH GREATER-THAN / MUCH GREATER THAN
U+0226B + U+00338 ≫̸            \\NotGreaterGreater        MUCH GREATER-THAN / MUCH GREATER THAN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+0226C           ≬            \\between                  BETWEEN
U+0226E           ≮            \\nless                    NOT LESS-THAN / NOT LESS THAN
U+0226F           ≯            \\ngtr                     NOT GREATER-THAN / NOT GREATER THAN
U+02270           ≰            \\nleq                     NEITHER LESS-THAN NOR EQUAL TO / NEITHER LESS THAN NOR EQUAL TO
U+02271           ≱            \\ngeq                     NEITHER GREATER-THAN NOR EQUAL TO / NEITHER GREATER THAN NOR EQUAL TO
U+02272           ≲            \\lesssim                  LESS-THAN OR EQUIVALENT TO / LESS THAN OR EQUIVALENT TO
U+02273           ≳            \\gtrsim                   GREATER-THAN OR EQUIVALENT TO / GREATER THAN OR EQUIVALENT TO
U+02276           ≶            \\lessgtr                  LESS-THAN OR GREATER-THAN / LESS THAN OR GREATER THAN
U+02277           ≷            \\gtrless                  GREATER-THAN OR LESS-THAN / GREATER THAN OR LESS THAN
U+02278           ≸            \\notlessgreater           NEITHER LESS-THAN NOR GREATER-THAN / NEITHER LESS THAN NOR GREATER THAN
U+02279           ≹            \\notgreaterless           NEITHER GREATER-THAN NOR LESS-THAN / NEITHER GREATER THAN NOR LESS THAN
U+0227A           ≺            \\prec                     PRECEDES
U+0227B           ≻            \\succ                     SUCCEEDS
U+0227C           ≼            \\preccurlyeq              PRECEDES OR EQUAL TO
U+0227D           ≽            \\succcurlyeq              SUCCEEDS OR EQUAL TO
U+0227E           ≾            \\precsim                  PRECEDES OR EQUIVALENT TO
U+0227E + U+00338 ≾̸            \\nprecsim                 PRECEDES OR EQUIVALENT TO + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+0227F           ≿            \\succsim                  SUCCEEDS OR EQUIVALENT TO
U+0227F + U+00338 ≿̸            \\nsuccsim                 SUCCEEDS OR EQUIVALENT TO + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02280           ⊀            \\nprec                    DOES NOT PRECEDE
U+02281           ⊁            \\nsucc                    DOES NOT SUCCEED
U+02282           ⊂            \\subset                   SUBSET OF
U+02283           ⊃            \\supset                   SUPERSET OF
U+02284           ⊄            \\nsubset                  NOT A SUBSET OF
U+02285           ⊅            \\nsupset                  NOT A SUPERSET OF
U+02286           ⊆            \\subseteq                 SUBSET OF OR EQUAL TO
U+02287           ⊇            \\supseteq                 SUPERSET OF OR EQUAL TO
U+02288           ⊈            \\nsubseteq                NEITHER A SUBSET OF NOR EQUAL TO
U+02289           ⊉            \\nsupseteq                NEITHER A SUPERSET OF NOR EQUAL TO
U+0228A           ⊊            \\subsetneq                SUBSET OF WITH NOT EQUAL TO / SUBSET OF OR NOT EQUAL TO
U+0228A + U+0FE00 ⊊︀            \\varsubsetneqq            SUBSET OF WITH NOT EQUAL TO / SUBSET OF OR NOT EQUAL TO + VARIATION SELECTOR-1
U+0228B           ⊋            \\supsetneq                SUPERSET OF WITH NOT EQUAL TO / SUPERSET OF OR NOT EQUAL TO
U+0228B + U+0FE00 ⊋︀            \\varsupsetneq             SUPERSET OF WITH NOT EQUAL TO / SUPERSET OF OR NOT EQUAL TO + VARIATION SELECTOR-1
U+0228D           ⊍            \\cupdot                   MULTISET MULTIPLICATION
U+0228E           ⊎            \\uplus                    MULTISET UNION
U+0228F           ⊏            \\sqsubset                 SQUARE IMAGE OF
U+0228F + U+00338 ⊏̸            \\NotSquareSubset          SQUARE IMAGE OF + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02290           ⊐            \\sqsupset                 SQUARE ORIGINAL OF
U+02290 + U+00338 ⊐̸            \\NotSquareSuperset        SQUARE ORIGINAL OF + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02291           ⊑            \\sqsubseteq               SQUARE IMAGE OF OR EQUAL TO
U+02292           ⊒            \\sqsupseteq               SQUARE ORIGINAL OF OR EQUAL TO
U+02293           ⊓            \\sqcap                    SQUARE CAP
U+02294           ⊔            \\sqcup                    SQUARE CUP
U+02295           ⊕            \\oplus                    CIRCLED PLUS
U+02296           ⊖            \\ominus                   CIRCLED MINUS
U+02297           ⊗            \\otimes                   CIRCLED TIMES
U+02298           ⊘            \\oslash                   CIRCLED DIVISION SLASH
U+02299           ⊙            \\odot                     CIRCLED DOT OPERATOR
U+0229A           ⊚            \\circledcirc              CIRCLED RING OPERATOR
U+0229B           ⊛            \\circledast               CIRCLED ASTERISK OPERATOR
U+0229D           ⊝            \\circleddash              CIRCLED DASH
U+0229E           ⊞            \\boxplus                  SQUARED PLUS
U+0229F           ⊟            \\boxminus                 SQUARED MINUS
U+022A0           ⊠            \\boxtimes                 SQUARED TIMES
U+022A1           ⊡            \\boxdot                   SQUARED DOT OPERATOR
U+022A2           ⊢            \\vdash                    RIGHT TACK
U+022A3           ⊣            \\dashv                    LEFT TACK
U+022A4           ⊤            \\top                      DOWN TACK
U+022A5           ⊥            \\perp                     UP TACK
U+022A7           ⊧            \\models                   MODELS
U+022A8           ⊨            \\vDash                    TRUE
U+022A9           ⊩            \\Vdash                    FORCES
U+022AA           ⊪            \\Vvdash                   TRIPLE VERTICAL BAR RIGHT TURNSTILE
U+022AB           ⊫            \\VDash                    DOUBLE VERTICAL BAR DOUBLE RIGHT TURNSTILE
U+022AC           ⊬            \\nvdash                   DOES NOT PROVE
U+022AD           ⊭            \\nvDash                   NOT TRUE
U+022AE           ⊮            \\nVdash                   DOES NOT FORCE
U+022AF           ⊯            \\nVDash                   NEGATED DOUBLE VERTICAL BAR DOUBLE RIGHT TURNSTILE
U+022B2           ⊲            \\vartriangleleft          NORMAL SUBGROUP OF
U+022B3           ⊳            \\vartriangleright         CONTAINS AS NORMAL SUBGROUP
U+022B4           ⊴            \\trianglelefteq           NORMAL SUBGROUP OF OR EQUAL TO
U+022B5           ⊵            \\trianglerighteq          CONTAINS AS NORMAL SUBGROUP OR EQUAL TO
U+022B6           ⊶            \\original                 ORIGINAL OF
U+022B7           ⊷            \\image                    IMAGE OF
U+022B8           ⊸            \\multimap                 MULTIMAP
U+022B9           ⊹            \\hermitconjmatrix         HERMITIAN CONJUGATE MATRIX
U+022BA           ⊺            \\intercal                 INTERCALATE
U+022BB           ⊻            \\veebar                   XOR
U+022BE           ⊾            \\rightanglearc            RIGHT ANGLE WITH ARC
U+022C0           ⋀            \\bigwedge                 N-ARY LOGICAL AND
U+022C1           ⋁            \\bigvee                   N-ARY LOGICAL OR
U+022C2           ⋂            \\bigcap                   N-ARY INTERSECTION
U+022C3           ⋃            \\bigcup                   N-ARY UNION
U+022C4           ⋄            \\diamond                  DIAMOND OPERATOR
U+022C5           ⋅            \\cdot                     DOT OPERATOR
U+022C6           ⋆            \\star                     STAR OPERATOR
U+022C7           ⋇            \\divideontimes            DIVISION TIMES
U+022C8           ⋈            \\bowtie                   BOWTIE
U+022C9           ⋉            \\ltimes                   LEFT NORMAL FACTOR SEMIDIRECT PRODUCT
U+022CA           ⋊            \\rtimes                   RIGHT NORMAL FACTOR SEMIDIRECT PRODUCT
U+022CB           ⋋            \\leftthreetimes           LEFT SEMIDIRECT PRODUCT
U+022CC           ⋌            \\rightthreetimes          RIGHT SEMIDIRECT PRODUCT
U+022CD           ⋍            \\backsimeq                REVERSED TILDE EQUALS
U+022CE           ⋎            \\curlyvee                 CURLY LOGICAL OR
U+022CF           ⋏            \\curlywedge               CURLY LOGICAL AND
U+022D0           ⋐            \\Subset                   DOUBLE SUBSET
U+022D1           ⋑            \\Supset                   DOUBLE SUPERSET
U+022D2           ⋒            \\Cap                      DOUBLE INTERSECTION
U+022D3           ⋓            \\Cup                      DOUBLE UNION
U+022D4           ⋔            \\pitchfork                PITCHFORK
U+022D6           ⋖            \\lessdot                  LESS-THAN WITH DOT / LESS THAN WITH DOT
U+022D7           ⋗            \\gtrdot                   GREATER-THAN WITH DOT / GREATER THAN WITH DOT
U+022D8           ⋘            \\verymuchless             VERY MUCH LESS-THAN / VERY MUCH LESS THAN
U+022D9           ⋙            \\ggg                      VERY MUCH GREATER-THAN / VERY MUCH GREATER THAN
U+022DA           ⋚            \\lesseqgtr                LESS-THAN EQUAL TO OR GREATER-THAN / LESS THAN EQUAL TO OR GREATER THAN
U+022DB           ⋛            \\gtreqless                GREATER-THAN EQUAL TO OR LESS-THAN / GREATER THAN EQUAL TO OR LESS THAN
U+022DE           ⋞            \\curlyeqprec              EQUAL TO OR PRECEDES
U+022DF           ⋟            \\curlyeqsucc              EQUAL TO OR SUCCEEDS
U+022E5           ⋥            \\Elzsqspne                SQUARE ORIGINAL OF OR NOT EQUAL TO
U+022E6           ⋦            \\lnsim                    LESS-THAN BUT NOT EQUIVALENT TO / LESS THAN BUT NOT EQUIVALENT TO
U+022E7           ⋧            \\gnsim                    GREATER-THAN BUT NOT EQUIVALENT TO / GREATER THAN BUT NOT EQUIVALENT TO
U+022E8           ⋨            \\precnsim                 PRECEDES BUT NOT EQUIVALENT TO
U+022E9           ⋩            \\succnsim                 SUCCEEDS BUT NOT EQUIVALENT TO
U+022EA           ⋪            \\ntriangleleft            NOT NORMAL SUBGROUP OF
U+022EB           ⋫            \\ntriangleright           DOES NOT CONTAIN AS NORMAL SUBGROUP
U+022EC           ⋬            \\ntrianglelefteq          NOT NORMAL SUBGROUP OF OR EQUAL TO
U+022ED           ⋭            \\ntrianglerighteq         DOES NOT CONTAIN AS NORMAL SUBGROUP OR EQUAL
U+022EE           ⋮            \\vdots                    VERTICAL ELLIPSIS
U+022EF           ⋯            \\cdots                    MIDLINE HORIZONTAL ELLIPSIS
U+022F0           ⋰            \\adots                    UP RIGHT DIAGONAL ELLIPSIS
U+022F1           ⋱            \\ddots                    DOWN RIGHT DIAGONAL ELLIPSIS
U+02305           ⌅            \\barwedge                 PROJECTIVE
U+02308           ⌈            \\lceil                    LEFT CEILING
U+02309           ⌉            \\rceil                    RIGHT CEILING
U+0230A           ⌊            \\lfloor                   LEFT FLOOR
U+0230B           ⌋            \\rfloor                   RIGHT FLOOR
U+02315           ⌕            \\recorder                 TELEPHONE RECORDER
U+0231C           ⌜            \\ulcorner                 TOP LEFT CORNER
U+0231D           ⌝            \\urcorner                 TOP RIGHT CORNER
U+0231E           ⌞            \\llcorner                 BOTTOM LEFT CORNER
U+0231F           ⌟            \\lrcorner                 BOTTOM RIGHT CORNER
U+02322           ⌢            \\frown                    FROWN
U+02323           ⌣            \\smile                    SMILE
U+0233D           ⌽            \\obar                     APL FUNCTIONAL SYMBOL CIRCLE STILE
U+023A3           ⎣            \\Elzdlcorn                LEFT SQUARE BRACKET LOWER CORNER
U+023B0           ⎰            \\lmoustache               UPPER LEFT OR LOWER RIGHT CURLY BRACKET SECTION
U+023B1           ⎱            \\rmoustache               UPPER RIGHT OR LOWER LEFT CURLY BRACKET SECTION
U+02423           ␣            \\textvisiblespace         OPEN BOX
U+024C8           Ⓢ            \\circledS                 CIRCLED LATIN CAPITAL LETTER S
U+02506           ┆            \\Elzdshfnc                BOX DRAWINGS LIGHT TRIPLE DASH VERTICAL / FORMS LIGHT TRIPLE DASH VERTICAL
U+02519           ┙            \\Elzsqfnw                 BOX DRAWINGS UP LIGHT AND LEFT HEAVY / FORMS UP LIGHT AND LEFT HEAVY
U+02571           ╱            \\diagup                   BOX DRAWINGS LIGHT DIAGONAL UPPER RIGHT TO LOWER LEFT / FORMS LIGHT DIAGONAL UPPER RIGHT TO LOWER LEFT
U+02572           ╲            \\diagdown                 BOX DRAWINGS LIGHT DIAGONAL UPPER LEFT TO LOWER RIGHT / FORMS LIGHT DIAGONAL UPPER LEFT TO LOWER RIGHT
U+025A0           ■            \\blacksquare              BLACK SQUARE
U+025A1           □            \\square                   WHITE SQUARE
U+025AF           ▯            \\Elzvrecto                WHITE VERTICAL RECTANGLE
U+025B3           △            \\bigtriangleup            WHITE UP-POINTING TRIANGLE / WHITE UP POINTING TRIANGLE
U+025B4           ▴            \\blacktriangle            BLACK UP-POINTING SMALL TRIANGLE / BLACK UP POINTING SMALL TRIANGLE
U+025B5           ▵            \\vartriangle              WHITE UP-POINTING SMALL TRIANGLE / WHITE UP POINTING SMALL TRIANGLE
U+025B8           ▸            \\blacktriangleright       BLACK RIGHT-POINTING SMALL TRIANGLE / BLACK RIGHT POINTING SMALL TRIANGLE
U+025B9           ▹            \\triangleright            WHITE RIGHT-POINTING SMALL TRIANGLE / WHITE RIGHT POINTING SMALL TRIANGLE
U+025BD           ▽            \\bigtriangledown          WHITE DOWN-POINTING TRIANGLE / WHITE DOWN POINTING TRIANGLE
U+025BE           ▾            \\blacktriangledown        BLACK DOWN-POINTING SMALL TRIANGLE / BLACK DOWN POINTING SMALL TRIANGLE
U+025BF           ▿            \\triangledown             WHITE DOWN-POINTING SMALL TRIANGLE / WHITE DOWN POINTING SMALL TRIANGLE
U+025C2           ◂            \\blacktriangleleft        BLACK LEFT-POINTING SMALL TRIANGLE / BLACK LEFT POINTING SMALL TRIANGLE
U+025C3           ◃            \\triangleleft             WHITE LEFT-POINTING SMALL TRIANGLE / WHITE LEFT POINTING SMALL TRIANGLE
U+025CA           ◊            \\lozenge                  LOZENGE
U+025CB           ○            \\bigcirc                  WHITE CIRCLE
U+025D0           ◐            \\Elzcirfl                 CIRCLE WITH LEFT HALF BLACK
U+025D1           ◑            \\Elzcirfr                 CIRCLE WITH RIGHT HALF BLACK
U+025D2           ◒            \\Elzcirfb                 CIRCLE WITH LOWER HALF BLACK
U+025D8           ◘            \\Elzrvbull                INVERSE BULLET
U+025E7           ◧            \\Elzsqfl                  SQUARE WITH LEFT HALF BLACK
U+025E8           ◨            \\Elzsqfr                  SQUARE WITH RIGHT HALF BLACK
U+025EA           ◪            \\Elzsqfse                 SQUARE WITH LOWER RIGHT DIAGONAL HALF BLACK
U+02605           ★            \\bigstar                  BLACK STAR
U+0263E           ☾            \\rightmoon                LAST QUARTER MOON
U+0263F           ☿            \\mercury                  MERCURY
U+02640           ♀            \\female, \\venus          FEMALE SIGN
U+02642           ♂            \\male, \\mars             MALE SIGN
U+02643           ♃            \\jupiter                  JUPITER
U+02644           ♄            \\saturn                   SATURN
U+02645           ♅            \\uranus                   URANUS
U+02646           ♆            \\neptune                  NEPTUNE
U+02647           ♇            \\pluto                    PLUTO
U+02648           ♈            \\aries                    ARIES
U+02649           ♉            \\taurus                   TAURUS
U+0264A           ♊            \\gemini                   GEMINI
U+0264B           ♋            \\cancer                   CANCER
U+0264C           ♌            \\leo                      LEO
U+0264D           ♍            \\virgo                    VIRGO
U+0264E           ♎            \\libra                    LIBRA
U+0264F           ♏            \\scorpio                  SCORPIUS
U+02650           ♐            \\sagittarius              SAGITTARIUS
U+02651           ♑            \\capricornus              CAPRICORN
U+02652           ♒            \\aquarius                 AQUARIUS
U+02653           ♓            \\pisces                   PISCES
U+02660           ♠            \\spadesuit                BLACK SPADE SUIT
U+02661           ♡            \\heartsuit                WHITE HEART SUIT
U+02662           ♢            \\diamondsuit              WHITE DIAMOND SUIT
U+02663           ♣            \\clubsuit                 BLACK CLUB SUIT
U+02669           ♩            \\quarternote              QUARTER NOTE
U+0266A           ♪            \\eighthnote               EIGHTH NOTE
U+0266D           ♭            \\flat                     MUSIC FLAT SIGN / FLAT
U+0266E           ♮            \\natural                  MUSIC NATURAL SIGN / NATURAL
U+0266F           ♯            \\sharp                    MUSIC SHARP SIGN / SHARP
U+02713           ✓            \\checkmark                CHECK MARK
U+02720           ✠            \\maltese                  MALTESE CROSS
U+027E8           ⟨            \\langle                   MATHEMATICAL LEFT ANGLE BRACKET
U+027E9           ⟩            \\rangle                   MATHEMATICAL RIGHT ANGLE BRACKET
U+027F5           ⟵            \\longleftarrow            LONG LEFTWARDS ARROW
U+027F6           ⟶            \\longrightarrow           LONG RIGHTWARDS ARROW
U+027F7           ⟷            \\longleftrightarrow       LONG LEFT RIGHT ARROW
U+027F8           ⟸            \\Longleftarrow            LONG LEFTWARDS DOUBLE ARROW
U+027F9           ⟹            \\Longrightarrow           LONG RIGHTWARDS DOUBLE ARROW
U+027FA           ⟺            \\Longleftrightarrow       LONG LEFT RIGHT DOUBLE ARROW
U+027FC           ⟼            \\longmapsto               LONG RIGHTWARDS ARROW FROM BAR
U+02906           ⤆            \\Mapsfrom                 LEFTWARDS DOUBLE ARROW FROM BAR
U+02907           ⤇            \\Mapsto                   RIGHTWARDS DOUBLE ARROW FROM BAR
U+0290A           ⤊            \\Uuparrow                 UPWARDS TRIPLE ARROW
U+0290B           ⤋            \\Ddownarrow               DOWNWARDS TRIPLE ARROW
U+0290D           ⤍            \\bkarow                   RIGHTWARDS DOUBLE DASH ARROW
U+0290F           ⤏            \\dbkarow                  RIGHTWARDS TRIPLE DASH ARROW
U+02910           ⤐            \\drbkarrow                RIGHTWARDS TWO-HEADED TRIPLE DASH ARROW
U+02912           ⤒            \\UpArrowBar               UPWARDS ARROW TO BAR
U+02913           ⤓            \\DownArrowBar             DOWNWARDS ARROW TO BAR
U+02916           ⤖            \\twoheadrightarrowtail    RIGHTWARDS TWO-HEADED ARROW WITH TAIL
U+02925           ⤥            \\hksearow                 SOUTH EAST ARROW WITH HOOK
U+02926           ⤦            \\hkswarow                 SOUTH WEST ARROW WITH HOOK
U+02927           ⤧            \\tona                     NORTH WEST ARROW AND NORTH EAST ARROW
U+02928           ⤨            \\toea                     NORTH EAST ARROW AND SOUTH EAST ARROW
U+02929           ⤩            \\tosa                     SOUTH EAST ARROW AND SOUTH WEST ARROW
U+0292A           ⤪            \\towa                     SOUTH WEST ARROW AND NORTH WEST ARROW
U+0292B           ⤫            \\rdiagovfdiag             RISING DIAGONAL CROSSING FALLING DIAGONAL
U+0292C           ⤬            \\fdiagovrdiag             FALLING DIAGONAL CROSSING RISING DIAGONAL
U+0292D           ⤭            \\seovnearrow              SOUTH EAST ARROW CROSSING NORTH EAST ARROW
U+0292E           ⤮            \\neovsearrow              NORTH EAST ARROW CROSSING SOUTH EAST ARROW
U+0292F           ⤯            \\fdiagovnearrow           FALLING DIAGONAL CROSSING NORTH EAST ARROW
U+02930           ⤰            \\rdiagovsearrow           RISING DIAGONAL CROSSING SOUTH EAST ARROW
U+02931           ⤱            \\neovnwarrow              NORTH EAST ARROW CROSSING NORTH WEST ARROW
U+02932           ⤲            \\nwovnearrow              NORTH WEST ARROW CROSSING NORTH EAST ARROW
U+02942           ⥂            \\ElzRlarr                 RIGHTWARDS ARROW ABOVE SHORT LEFTWARDS ARROW
U+02944           ⥄            \\ElzrLarr                 SHORT RIGHTWARDS ARROW ABOVE LEFTWARDS ARROW
U+02947           ⥇            \\Elzrarrx                 RIGHTWARDS ARROW THROUGH X
U+0294E           ⥎            \\LeftRightVector          LEFT BARB UP RIGHT BARB UP HARPOON
U+0294F           ⥏            \\RightUpDownVector        UP BARB RIGHT DOWN BARB RIGHT HARPOON
U+02950           ⥐            \\DownLeftRightVector      LEFT BARB DOWN RIGHT BARB DOWN HARPOON
U+02951           ⥑            \\LeftUpDownVector         UP BARB LEFT DOWN BARB LEFT HARPOON
U+02952           ⥒            \\LeftVectorBar            LEFTWARDS HARPOON WITH BARB UP TO BAR
U+02953           ⥓            \\RightVectorBar           RIGHTWARDS HARPOON WITH BARB UP TO BAR
U+02954           ⥔            \\RightUpVectorBar         UPWARDS HARPOON WITH BARB RIGHT TO BAR
U+02955           ⥕            \\RightDownVectorBar       DOWNWARDS HARPOON WITH BARB RIGHT TO BAR
U+02956           ⥖            \\DownLeftVectorBar        LEFTWARDS HARPOON WITH BARB DOWN TO BAR
U+02957           ⥗            \\DownRightVectorBar       RIGHTWARDS HARPOON WITH BARB DOWN TO BAR
U+02958           ⥘            \\LeftUpVectorBar          UPWARDS HARPOON WITH BARB LEFT TO BAR
U+02959           ⥙            \\LeftDownVectorBar        DOWNWARDS HARPOON WITH BARB LEFT TO BAR
U+0295A           ⥚            \\LeftTeeVector            LEFTWARDS HARPOON WITH BARB UP FROM BAR
U+0295B           ⥛            \\RightTeeVector           RIGHTWARDS HARPOON WITH BARB UP FROM BAR
U+0295C           ⥜            \\RightUpTeeVector         UPWARDS HARPOON WITH BARB RIGHT FROM BAR
U+0295D           ⥝            \\RightDownTeeVector       DOWNWARDS HARPOON WITH BARB RIGHT FROM BAR
U+0295E           ⥞            \\DownLeftTeeVector        LEFTWARDS HARPOON WITH BARB DOWN FROM BAR
U+0295F           ⥟            \\DownRightTeeVector       RIGHTWARDS HARPOON WITH BARB DOWN FROM BAR
U+02960           ⥠            \\LeftUpTeeVector          UPWARDS HARPOON WITH BARB LEFT FROM BAR
U+02961           ⥡            \\LeftDownTeeVector        DOWNWARDS HARPOON WITH BARB LEFT FROM BAR
U+0296E           ⥮            \\UpEquilibrium            UPWARDS HARPOON WITH BARB LEFT BESIDE DOWNWARDS HARPOON WITH BARB RIGHT
U+0296F           ⥯            \\ReverseUpEquilibrium     DOWNWARDS HARPOON WITH BARB LEFT BESIDE UPWARDS HARPOON WITH BARB RIGHT
U+02970           ⥰            \\RoundImplies             RIGHT DOUBLE ARROW WITH ROUNDED HEAD
U+02980           ⦀            \\Vvert                    TRIPLE VERTICAL BAR DELIMITER
U+02986           ⦆            \\Elroang                  RIGHT WHITE PARENTHESIS
U+02999           ⦙            \\Elzddfnc                 DOTTED FENCE
U+0299C           ⦜            \\Angle                    RIGHT ANGLE VARIANT WITH SQUARE
U+029A0           ⦠            \\Elzlpargt                SPHERICAL ANGLE OPENING LEFT
U+029B8           ⦸            \\obslash                  CIRCLED REVERSE SOLIDUS
U+029C4           ⧄            \\boxdiag                  SQUARED RISING DIAGONAL SLASH
U+029C5           ⧅            \\boxbslash                SQUARED FALLING DIAGONAL SLASH
U+029C6           ⧆            \\boxast                   SQUARED ASTERISK
U+029C7           ⧇            \\boxcircle                SQUARED SMALL CIRCLE
U+029CA           ⧊            \\ElzLap                   TRIANGLE WITH DOT ABOVE
U+029CB           ⧋            \\Elzdefas                 TRIANGLE WITH UNDERBAR
U+029CF           ⧏            \\LeftTriangleBar          LEFT TRIANGLE BESIDE VERTICAL BAR
U+029CF + U+00338 ⧏̸            \\NotLeftTriangleBar       LEFT TRIANGLE BESIDE VERTICAL BAR + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+029D0           ⧐            \\RightTriangleBar         VERTICAL BAR BESIDE RIGHT TRIANGLE
U+029D0 + U+00338 ⧐̸            \\NotRightTriangleBar      VERTICAL BAR BESIDE RIGHT TRIANGLE + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+029DF           ⧟            \\dualmap                  DOUBLE-ENDED MULTIMAP
U+029E2           ⧢            \\shuffle                  SHUFFLE PRODUCT
U+029EB           ⧫            \\blacklozenge             BLACK LOZENGE
U+029F4           ⧴            \\RuleDelayed              RULE-DELAYED
U+02A00           ⨀            \\bigodot                  N-ARY CIRCLED DOT OPERATOR
U+02A01           ⨁            \\bigoplus                 N-ARY CIRCLED PLUS OPERATOR
U+02A02           ⨂            \\bigotimes                N-ARY CIRCLED TIMES OPERATOR
U+02A03           ⨃            \\bigcupdot                N-ARY UNION OPERATOR WITH DOT
U+02A04           ⨄            \\biguplus                 N-ARY UNION OPERATOR WITH PLUS
U+02A05           ⨅            \\bigsqcap                 N-ARY SQUARE INTERSECTION OPERATOR
U+02A06           ⨆            \\bigsqcup                 N-ARY SQUARE UNION OPERATOR
U+02A07           ⨇            \\conjquant                TWO LOGICAL AND OPERATOR
U+02A08           ⨈            \\disjquant                TWO LOGICAL OR OPERATOR
U+02A09           ⨉            \\bigtimes                 N-ARY TIMES OPERATOR
U+02A0C           ⨌            \\iiiint                   QUADRUPLE INTEGRAL OPERATOR
U+02A0D           ⨍            \\intbar                   FINITE PART INTEGRAL
U+02A0E           ⨎            \\intBar                   INTEGRAL WITH DOUBLE STROKE
U+02A0F           ⨏            \\clockoint                INTEGRAL AVERAGE WITH SLASH
U+02A16           ⨖            \\sqrint                   QUATERNION INTEGRAL OPERATOR
U+02A18           ⨘            \\intx                     INTEGRAL WITH TIMES SIGN
U+02A19           ⨙            \\intcap                   INTEGRAL WITH INTERSECTION
U+02A1A           ⨚            \\intcup                   INTEGRAL WITH UNION
U+02A1B           ⨛            \\upint                    INTEGRAL WITH OVERBAR
U+02A1C           ⨜            \\lowint                   INTEGRAL WITH UNDERBAR
U+02A25           ⨥            \\plusdot                  PLUS SIGN WITH DOT BELOW
U+02A2A           ⨪            \\minusdot                 MINUS SIGN WITH DOT BELOW
U+02A2F           ⨯            \\ElzTimes                 VECTOR OR CROSS PRODUCT
U+02A32           ⨲            \\btimes                   SEMIDIRECT PRODUCT WITH BOTTOM CLOSED
U+02A3C           ⨼            \\intprod                  INTERIOR PRODUCT
U+02A3D           ⨽            \\intprodr                 RIGHTHAND INTERIOR PRODUCT
U+02A3F           ⨿            \\amalg                    AMALGAMATION OR COPRODUCT
U+02A53           ⩓            \\ElzAnd                   DOUBLE LOGICAL AND
U+02A54           ⩔            \\ElzOr                    DOUBLE LOGICAL OR
U+02A56           ⩖            \\ElOr                     TWO INTERSECTING LOGICAL OR
U+02A5E           ⩞            \\perspcorrespond          LOGICAL AND WITH DOUBLE OVERBAR
U+02A5F           ⩟            \\Elzminhat                LOGICAL AND WITH UNDERBAR
U+02A75           ⩵            \\Equal                    TWO CONSECUTIVE EQUALS SIGNS
U+02A77           ⩷            \\ddotseq                  EQUALS SIGN WITH TWO DOTS ABOVE AND TWO DOTS BELOW
U+02A7D           ⩽            \\leqslant                 LESS-THAN OR SLANTED EQUAL TO
U+02A7D + U+00338 ⩽̸            \\nleqslant                LESS-THAN OR SLANTED EQUAL TO + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02A7E           ⩾            \\geqslant                 GREATER-THAN OR SLANTED EQUAL TO
U+02A7E + U+00338 ⩾̸            \\ngeqslant                GREATER-THAN OR SLANTED EQUAL TO + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02A85           ⪅            \\lessapprox               LESS-THAN OR APPROXIMATE
U+02A86           ⪆            \\gtrapprox                GREATER-THAN OR APPROXIMATE
U+02A87           ⪇            \\lneq                     LESS-THAN AND SINGLE-LINE NOT EQUAL TO
U+02A88           ⪈            \\gneq                     GREATER-THAN AND SINGLE-LINE NOT EQUAL TO
U+02A89           ⪉            \\lnapprox                 LESS-THAN AND NOT APPROXIMATE
U+02A8A           ⪊            \\gnapprox                 GREATER-THAN AND NOT APPROXIMATE
U+02A8B           ⪋            \\lesseqqgtr               LESS-THAN ABOVE DOUBLE-LINE EQUAL ABOVE GREATER-THAN
U+02A8C           ⪌            \\gtreqqless               GREATER-THAN ABOVE DOUBLE-LINE EQUAL ABOVE LESS-THAN
U+02A95           ⪕            \\eqslantless              SLANTED EQUAL TO OR LESS-THAN
U+02A96           ⪖            \\eqslantgtr               SLANTED EQUAL TO OR GREATER-THAN
U+02AA1           ⪡            \\NestedLessLess           DOUBLE NESTED LESS-THAN
U+02AA1 + U+00338 ⪡̸            \\NotNestedLessLess        DOUBLE NESTED LESS-THAN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02AA2           ⪢            \\NestedGreaterGreater     DOUBLE NESTED GREATER-THAN
U+02AA2 + U+00338 ⪢̸            \\NotNestedGreaterGreater  DOUBLE NESTED GREATER-THAN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02AA3           ⪣            \\partialmeetcontraction   DOUBLE NESTED LESS-THAN WITH UNDERBAR
U+02AAE           ⪮            \\bumpeqq                  EQUALS SIGN WITH BUMPY ABOVE
U+02AAF           ⪯            \\preceq                   PRECEDES ABOVE SINGLE-LINE EQUALS SIGN
U+02AAF + U+00338 ⪯̸            \\npreceq                  PRECEDES ABOVE SINGLE-LINE EQUALS SIGN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02AB0           ⪰            \\succeq                   SUCCEEDS ABOVE SINGLE-LINE EQUALS SIGN
U+02AB0 + U+00338 ⪰̸            \\nsucceq                  SUCCEEDS ABOVE SINGLE-LINE EQUALS SIGN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02AB5           ⪵            \\precneqq                 PRECEDES ABOVE NOT EQUAL TO
U+02AB6           ⪶            \\succneqq                 SUCCEEDS ABOVE NOT EQUAL TO
U+02AB7           ⪷            \\precapprox               PRECEDES ABOVE ALMOST EQUAL TO
U+02AB8           ⪸            \\succapprox               SUCCEEDS ABOVE ALMOST EQUAL TO
U+02AB9           ⪹            \\precnapprox              PRECEDES ABOVE NOT ALMOST EQUAL TO
U+02ABA           ⪺            \\succnapprox              SUCCEEDS ABOVE NOT ALMOST EQUAL TO
U+02AC5           ⫅            \\subseteqq                SUBSET OF ABOVE EQUALS SIGN
U+02AC5 + U+00338 ⫅̸            \\nsubseteqq               SUBSET OF ABOVE EQUALS SIGN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02AC6           ⫆            \\supseteqq                SUPERSET OF ABOVE EQUALS SIGN
U+02AC6 + U+00338 ⫆̸            \\nsupseteqq               SUPERSET OF ABOVE EQUALS SIGN + COMBINING LONG SOLIDUS OVERLAY / NON-SPACING LONG SLASH OVERLAY
U+02ACB           ⫋            \\subsetneqq               SUBSET OF ABOVE NOT EQUAL TO
U+02ACC           ⫌            \\supsetneqq               SUPERSET OF ABOVE NOT EQUAL TO
U+02ADB           ⫛            \\mlcp                     TRANSVERSAL INTERSECTION
U+02ADC           ⫝̸            \\forks                    FORKING
U+02ADD           ⫝            \\forksnot                 NONFORKING
U+02AE3           ⫣            \\dashV                    DOUBLE VERTICAL BAR LEFT TURNSTILE
U+02AE4           ⫤            \\Dashv                    VERTICAL BAR DOUBLE LEFT TURNSTILE
U+02AF4           ⫴            \\interleave               TRIPLE VERTICAL BAR BINARY RELATION
U+02AF6           ⫶            \\Elztdcol                 TRIPLE COLON OPERATOR
U+02C7C           ⱼ            \\_j                       LATIN SUBSCRIPT SMALL LETTER J
U+02C7D           ⱽ            \\^V                       MODIFIER LETTER CAPITAL V
U+0301A           〚           \\openbracketleft          LEFT WHITE SQUARE BRACKET / OPENING WHITE SQUARE BRACKET
U+0301B           〛           \\openbracketright         RIGHT WHITE SQUARE BRACKET / CLOSING WHITE SQUARE BRACKET
U+0FE37           ︷           \\overbrace                PRESENTATION FORM FOR VERTICAL LEFT CURLY BRACKET / GLYPH FOR VERTICAL OPENING CURLY BRACKET
U+0FE38           ︸           \\underbrace               PRESENTATION FORM FOR VERTICAL RIGHT CURLY BRACKET / GLYPH FOR VERTICAL CLOSING CURLY BRACKET
================= ============ ========================== ========================================================================================================
