#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: multimedia.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import os
import sys
from reproductor import  Rhythmbox,Banshee 
from speak import Reproducir

def Multimedia():
	mediaplayer=""
	if os.system('ps xa | grep -v grep | grep banshee >/dev/null') == 0:
		mediaplayer = Banshee()
	elif os.system('ps xa | grep -v grep | grep rhythmbox >/dev/null') == 0:
		mediaplayer = Rhythmbox()
	elif os.system('which banshee >/dev/null') == 0:
		mediaplayer = Banshee()
		os.system('bash -c "nohup banshee >/dev/null 2>&1 < &1 & disown %%"')
	elif os.system('which rhythmbox >/dev/null') == 0:
		mediaplayer = Rhythmbox()
	else:
		Reproducir('No se encuentra instalado ningun reproductor. ' \
				'Por favor debe instalar Rhythmbox o Banshee.')
		sys.exit(1)

	return mediaplayer
