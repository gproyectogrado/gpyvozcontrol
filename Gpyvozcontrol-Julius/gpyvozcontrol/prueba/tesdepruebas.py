#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: tesdepruebas.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import os

os.system("HVite -T 1 -C ../../../input_files/config -H ../../hmm15/macros -H ../../hmm15/hmmdefs -S tes.scp \
-l '*' -i reconocimiento -w ../RedPalabras \
-p 0.0 -s 5.0 ../../../lexicon/VoxForgeDict.txt ../../tiedlist")

os.system('HResults -I tes.mlf ../../tiedlist reconocimiento')

