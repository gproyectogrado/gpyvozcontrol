#! /bin/bash
# coding: utf-8
####################################################################
###
### archivo: corridaTes.sh
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

echo "Script para ejecutar todos los tes"
echo ""
echo "Corriendo la prueba 1"
cd prueba1/
python ../tesdepruebas.py > ../resultado1
cd ..

echo "Corriendo la prueba 2"
cd prueba2/
python ../tesdepruebas.py > ../resultado2
cd ..

echo "Corriendo la prueba 3"
cd prueba3/
python ../tesdepruebas.py > ../resultado3
cd ..
