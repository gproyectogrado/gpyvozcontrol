#! /usr/bin/python -u
# coding: utf-8
####################################################################
###
### archivo: gpyvozcontrol.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import sys
import os
import pyatspi
import wnck
import pygtk
import gtk
import time
import locale
from multimedia import Multimedia
from aplicaciones import Aplicaciones, Accesibilidad, Carpetas
from evince import Evince
from speak import Reproducir
from sistema import Sistema
from clientecorreo import ClienteDeCorreo

class CommandAndControl:
	def __init__(self, file_object):

		self.mediaplayer = Multimedia()
		self.aplications = Aplicaciones()
		self.folders = Carpetas()
		self.evince = Evince()
		self.system = Sistema()
		self.correo = ClienteDeCorreo()

		os.system('amixer sset "Capture" 49800')
		print 'GPYVOZCONTROL - Sistema de órdenes de voz en español con Julius'
		Reproducir('Bienvenido a GPYVOZCONTROL, el sistema de órdenes de voz en español con Julius')
		for i in range(1, 4):
			Reproducir('%s' % str(4-i))
			time.sleep(1)
		Reproducir('Listopara recibir órdenes, ya puedes comenzar')

		startstring = 'sentence1: <s> '
		endstring = ' </s>'

		while 1:
			line = file_object.readline()
			screen = None
			screen = wnck.screen_get_default()

			if not line:
				break
			if 'missing phones' in line.lower():
				print 'Error: Missing phonemes for the used grammar file.'
				sys.exit(1)
			if line.startswith(startstring) and line.strip().endswith(endstring):
				self.parse(line.strip('\n')[len(startstring):-len(endstring)], screen)

	def parse(self, line, screen):
		# Parse the input
		params = [param.lower() for param in line.split() if param]
		if not '-q' in sys.argv and not '--quiet' in sys.argv:
			print 'Entrada reconocida:', ' '. join(params).capitalize()
			f = open('.orden', 'r')
			self.k = int(f.read())
			f.close()

		# Activa la bandera
		if params[-1] == 'vozcontrol':
			os.system('aplay .wavs/tono2.wav &')
			os.system('echo 1 > .orden')

		# Ejecuta el comando si es reconocido.
		if self.k == 1:
			Reproducir('Espera un momento')
			if params[-1] == 'reproductor':
				command = self.mediaplayer.parse(params[0])
				if params[0] == 'abrir' or params[0] == 'abre':
					os.system(command)
					Reproducir('%s fue activado' % self.mediaplayer.name)
				elif command:
					if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
						os.system(command)
						Reproducir('%s %s a %s' % (params[0], params[1], self.mediaplayer.name))
					else:
						print'No tienes un reproductor abierto para realizar esa acción'
						Reproducir('No tienes un reproductor abierto para realizar esa acción')
				elif not '-q' in sys.argv and not '--quiet' in sys.argv:
					print 'Acción no válida para %s' % self.mediaplayer.name
					Reproducir('Acción no válida para %s' % self.mediaplayer.name)
			elif params[0] == 'cancion':
				command = self.mediaplayer.parse(params[-1])
				if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
					os.system(command)
					Reproducir('Reproduciendo la %s %s' % (params[-1], params[0]))
				else:
					print'No tienes un reproductor abierto'
					Reproducir('No tienes un reproductor abierto para realizar esa acción')
			elif params[-1] == 'cancion':
				command = self.mediaplayer.parse(params[0])
				if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
					if params[0] == 'próximo' or params[0] == 'cambiar':
						os.system(command)
						Reproducir('Se cambia la canción')
					else:
						print "No entiendo la orden"
						Reproducir("Ups! no entiendo la orden")
				else:
					print'No tienes un reproductor abierto'
					Reproducir('No tienes un reproductor abierto para realizar esa acción')
			elif params[0] == 'dime' or params[0] == 'decir':
				self.SayDate(params[-1])
			elif params[0] == 'titulo':
				if params[-1] == 'cancion':
					command = self.mediaplayer.parse(params[0])
					if os.system('ps -A | grep rhythmbox > /dev/null') == 0 or os.system('ps -A | grep banshee > /dev/null') == 0:
						titulo = commands.getoutput(command)
						Reproducir(str(titulo))
					else:
						print'No tienes un reproductor abierto'
						Reproducir('No tienes un reproductor abierto para realizar esa acción')
				else:
					self.Actions(self.Acciones(screen), 'titulo')
			elif params[0] == 'abrir' or params[0] == 'abre':
				if params[1] == 'carpeta':
					command = self.folders.parse(params[-1])
					os.system(command)
					Reproducir('Carpeta %s fue abierta' % params[-1])
				elif params[-1] == 'correo':
					command = self.correo.parse(params[0])
					os.system(command)
					Reproducir('El cliente de correo %s fue abierto' % self.correo.name)
				else:
					command = self.aplications.parse(params[-1])
					os.system(command)
					Reproducir('La aplicación %s fue abierta' % params[-1])
			elif params[-1] == 'sistema' or params[-1] == 'equipo':
				command = self.system.parse(params[0])
				os.system(command)
				if params[0] == 'apagar' or params[0] == 'reiniciar':
					Reproducir('El %s se va a %s' % (params[-1], params[0]))
				else:
					#texto = '%s %s al %s' % (params[0], params[1], params[-1])
					Reproducir('%s %s al %s' % (params[0], params[1], params[-1]))
			elif params[0] == 'presentar':
				os.system('evince -f -s /home/joenco/Entrega-1.pdf &')
				Reproducir('Proyecto de grado fue abierto')
			elif params[0] == 'lamina':
				command = self.evince.parse(params[-1])
				if os.system('ps -A | grep evince > /dev/null') == 0:
					os.system(command[0])
					Reproducir(command[1])
				else:
					print'No tienes un Evince abierto'
					Reproducir('No tienes un Evince abierto')
			elif params[-1] == 'lamina':
				command = self.evince.parse(params[0])
				if os.system('ps -A | grep evince > /dev/null') == 0:
					os.system(command[0])
					Reproducir(command[1])
				else:
					print'No tienes un Evince abierto'
					Reproducir('No tienes un Evince abierto')
			elif params[0] == 'maximizar' or params[0] == 'maximiza':
				if params[-1] != 'maximiza' or params[-1] != 'maximizar':
					if params[1] == 'todas' or params[1] == 'todo':
						self.Actions(self.Acciones(screen), 'maximize_all')
						return True
					else:
						self.Actions(self.Acciones(screen), 'maximizar')
						return True
				else:
					self.Actions(self.Acciones(screen), 'maximizar')
					return True
			elif params[0] == 'minimizar' or params[0] == 'minimiza':
				if params[-1] != 'minimiza' or params[-1] != 'minimizar':
					if params[1] == 'todas' or params[1] == 'todo':
						self.Actions(self.Acciones(screen), 'minimize_all')
						return True
					else:
						self.Actions(self.Acciones(screen), 'minimizar')
						return True
				else:
					self.Actions(self.Acciones(screen), 'minimizar')
					return True
			elif params[0] == 'ampliar' or params[0] == 'amplia':
				if params[-1] != 'amplia' or params[-1] != 'ampliar':
					if params[1] == 'todas' or params[1] == 'todo':
						self.Actions(self.Acciones(screen), 'ampliar_all')
						return True
					else:
						self.Actions(self.Acciones(screen), 'ampliar')
						return True
				else:
					self.Actions(self.Acciones(screen), 'ampliar')
					return True
			elif params[0] == 'reducir' or params[0] == 'reduce':
				if params[-1] != 'reduce' or params[-1] != 'reducir':
					if params[1] == 'todas' or params[1] == 'todo':
						self.Actions(self.Acciones(screen), 'reduce_all')
						return True
					else:
						self.Actions(self.Acciones(screen), 'reducir')
						return True
				else:
					self.Actions(self.Acciones(screen), 'reducir')
					return True
			elif params[0] == 'cerrar' or params[0] == 'cierra':
				self.Actions(self.Acciones(screen), 'cerrar')
				return True
			else:
				print '¡Ups! No entiendo la orden'
				Reproducir('¡Ups! No entiendo la orden')
			os.system('echo 0 > .orden')
		screen = None

	def Acciones(self, screen):
		while gtk.events_pending():
			gtk.main_iteration()
			print "... force update ..."
			ventana = wnck.Screen.force_update(screen)
			print ventana 
			print "... force update ..."
			windows = screen.get_windows()
			for w in windows:
				return windows

	def Actions(self, windows, accion):
		for w in windows:
			window = wnck.Screen.get_active_window(w.get_screen())

		if accion == 'maximizar':
			win =  wnck.Screen.get_windows(wnck.screen_get_default())
			for w in win:
				w.unminimize(0)   	
			#window.unminimize(0)
			print "Maximizando la ventana "+window.get_name()
			Reproducir("Se maximiza la ventana "+window.get_name())
		if accion == 'minimizar':
			window.minimize()
			print "Minimizando la ventana "+window.get_name()
			Reproducir("Se minimiza la ventana "+window.get_name())
		if accion == 'cerrar':
			#window.close(int(time.time()))
			win =  wnck.Screen.get_windows(wnck.screen_get_default())
			for w in win:
				window = wnck.Screen.get_active_window(w.get_screen())
			window.close(int(time.time()))
			print 'Cerrando la ventana '+window.get_name()
			Reproducir('Cerrando la ventana '+window.get_name())
		if accion == 'minimize_all':
			print "Minimizando todas las ventanas"
			for w in windows:
				w.minimize()
			Reproducir("Se minimizan todas las ventanas")
		if accion == 'maximise_all':
			print "Maximizando todas las ventanas"
			win =  wnck.Screen.get_windows(wnck.screen_get_default())
			for w in win:
				w.unminimize(0)
			Reproducir("Se maximizan todas las ventanas")
		if accion == 'ampliar':
			window.maximize()
			print "Ampliando la ventana "+window.get_name()
			Reproducir("Se amplía la ventana "+window.get_name())
		if accion == 'reducir':
			window.unmaximize()
			print "Se reduce la ventana "+window.get_name()
			Reproducir("Se reduce la ventana "+window.get_name())
		if accion == 'ampliar_all':
			print "Ampliando todas las ventanas"
			for w in windows:
				w.maximize(0)
			Reproducir('Se amplían todas las ventanas')
		if accion == 'reduce_all':
			print "Reduce todas las ventanas"
			for w in windows:
				w.unmaximize(0)
			Reproducir('Se Reducen todas las ventanas')
		if accion == 'titulo':
			win =  wnck.Screen.get_windows(wnck.screen_get_default())
			print "La ventana activa es "+window.get_name()
			Reproducir("La ventana activa es "+window.get_name())
			Reproducir
			#for w in win:

	def SayDate(self, date):
		locale.setlocale(locale.LC_ALL, "")
		if date == 'fecha':
			fecha = time.strftime("%A , %d de %B del %Y", time.localtime())
			print fecha
			Reproducir(fecha)
		if date == 'hora':
			hora = time.strftime("son las %I y %M minutos")
			print hora
			Reproducir(hora)

if __name__ == '__main__':
	try:
		CommandAndControl(sys.stdin)
	except KeyboardInterrupt:
		sys.exit(1)
