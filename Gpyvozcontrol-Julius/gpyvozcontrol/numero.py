#! /usr/bin/env python
# coding: utf-8

import re

def ConvertirNumero(linea):
	linea = re.sub('diez', '10', linea)
	linea = re.sub('veinte', '20', linea)
	linea = re.sub('treinta', '30', linea)
	linea = re.sub('cuarenta', '40', linea)
	linea = re.sub('cincuenta', '50', linea)
	linea = re.sub('sesenta', '60', linea)
	linea = re.sub('setenta', '70', linea)
	linea = re.sub('ochenta', '80', linea)
	linea = re.sub('noventa', '90', linea)
	linea = re.sub('once', '11', linea)
	linea = re.sub('doce', '12', linea)
	linea = re.sub('trece', '13', linea)
	linea = re.sub('catorce', '14', linea)
	linea = re.sub('quince', '15', linea)
	linea = re.sub('dieci seis', '16', linea)
	linea = re.sub('dieci siete', '17', linea)
	linea = re.sub('dieci ocho', '18', linea)
	linea = re.sub('dieci nueve', '19', linea)
	linea = re.sub('0 y uno', '1', linea)
	linea = re.sub('0 y dos', '2', linea)
	linea = re.sub('0 y tres', '3', linea)
	linea = re.sub('0 y cuatro', '4', linea)
	linea = re.sub('0 y cinco', '5', linea)
	linea = re.sub('0 y seis', '6', linea)
	linea = re.sub('0 y siete', '7', linea)
	linea = re.sub('0 y ocho', '8', linea)
	linea = re.sub('0 y nueve', '9', linea)
	linea = re.sub('uno', '1', linea)
	linea = re.sub('dos', '2', linea)
	linea = re.sub('tres', '3', linea)
	linea = re.sub('cuatro', '4', linea)
	linea = re.sub('cinco', '5', linea)
	linea = re.sub('seis', '6', linea)
	linea = re.sub('siete', '7', linea)
	linea = re.sub('ocho', '8', linea)
	linea = re.sub('nueve', '9', linea)

	return linea
