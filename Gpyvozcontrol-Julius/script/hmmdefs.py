#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: hmmdefs.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

file = open('monophones0', 'r')
file1 = open('hmm0/hmmdefs', 'w')
file2 = open('hmm0/proto')
linea = file2.readline()
text = ""
n=1

while linea != "":
  if n > 4:
    text += linea
  linea = file2.readline()
  n += 1
file2.close()

for line in file:
  if line[-1] == '\n':
    line = line[:-1]
  texto = "~h \""+line+"\""
  file1.write(texto+'\n')
  file1.write(text)

file1.close()
file.close()
