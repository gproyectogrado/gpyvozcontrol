#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: ruta_naudios.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import os

data = '../train/wav/'
data1 = '../train/mfcc/'
carpeta = [
    #'jacinto',
    #'Rosa',
    #'rodriguezd',
    #'rojasa',
    #'gonzalezr',
    #'aguirre',
    #'carlos',
    #'diego',
    #'edward',
    #'franciscoCast',
    #'hector',
    #'luis',
    #'mariana',
    #'melva',
    #'roger',
    #'petrizom',
    #'vergarak',
    #'josmar',
    #'ramirezf',
    #'helder',
    'maldonadoj',
]
f = open('codetrain.scp', 'a')
f1 = open('train.scp', 'a')

for a in carpeta:
  os.system('mkdir ../train/mfcc/'+a)
  f.write('../train/wav/'+a+'/09.wav ../train/mfcc/'+a+'/09.mfc\n')
  f.write('../train/wav/'+a+'/11.wav ../train/mfcc/'+a+'/11.mfc\n')
  f.write('../train/wav/'+a+'/24.wav ../train/mfcc/'+a+'/24.mfc\n')
  f.write('../train/wav/'+a+'/37.wav ../train/mfcc/'+a+'/37.mfc\n')
  f.write('../train/wav/'+a+'/38.wav ../train/mfcc/'+a+'/38.mfc\n')
  f.write('../train/wav/'+a+'/39.wav ../train/mfcc/'+a+'/39.mfc\n')
  f.write('../train/wav/'+a+'/41.wav ../train/mfcc/'+a+'/41.mfc\n')
  f.write('../train/wav/'+a+'/42.wav ../train/mfcc/'+a+'/42.mfc\n')
  f.write('../train/wav/'+a+'/43.wav ../train/mfcc/'+a+'/43.mfc\n')
  f.write('../train/wav/'+a+'/44.wav ../train/mfcc/'+a+'/44.mfc\n')
  f.write('../train/wav/'+a+'/45.wav ../train/mfcc/'+a+'/45.mfc\n')
  f.write('../train/wav/'+a+'/47.wav ../train/mfcc/'+a+'/47.mfc\n')
  f.write('../train/wav/'+a+'/53.wav ../train/mfcc/'+a+'/53.mfc\n')
  f.write('../train/wav/'+a+'/54.wav ../train/mfcc/'+a+'/54.mfc\n')
  f.write('../train/wav/'+a+'/55.wav ../train/mfcc/'+a+'/55.mfc\n')
  f.write('../train/wav/'+a+'/56.wav ../train/mfcc/'+a+'/56.mfc\n')
  f.write('../train/wav/'+a+'/57.wav ../train/mfcc/'+a+'/57.mfc\n')
  f.write('../train/wav/'+a+'/59.wav ../train/mfcc/'+a+'/59.mfc\n')
  f.write('../train/wav/'+a+'/60.wav ../train/mfcc/'+a+'/60.mfc\n')
  f.write('../train/wav/'+a+'/62.wav ../train/mfcc/'+a+'/62.mfc\n')
  f.write('../train/wav/'+a+'/68.wav ../train/mfcc/'+a+'/68.mfc\n')
  f.write('../train/wav/'+a+'/69.wav ../train/mfcc/'+a+'/69.mfc\n')
  f.write('../train/wav/'+a+'/72.wav ../train/mfcc/'+a+'/72.mfc\n')
  f.write('../train/wav/'+a+'/74.wav ../train/mfcc/'+a+'/74.mfc\n')
  f.write('../train/wav/'+a+'/75.wav ../train/mfcc/'+a+'/75.mfc\n')
  f.write('../train/wav/'+a+'/81.wav ../train/mfcc/'+a+'/81.mfc\n')
  f.write('../train/wav/'+a+'/83.wav ../train/mfcc/'+a+'/83.mfc\n')
  f.write('../train/wav/'+a+'/84.wav ../train/mfcc/'+a+'/84.mfc\n')
  f.write('../train/wav/'+a+'/90.wav ../train/mfcc/'+a+'/90.mfc\n')
  f.write('../train/wav/'+a+'/95.wav ../train/mfcc/'+a+'/95.mfc\n')
  f.write('../train/wav/'+a+'/99.wav ../train/mfcc/'+a+'/99.mfc\n')
  f.write('../train/wav/'+a+'/118.wav ../train/mfcc/'+a+'/118.mfc\n')
  f.write('../train/wav/'+a+'/119.wav ../train/mfcc/'+a+'/119.mfc\n')
  f.write('../train/wav/'+a+'/123.wav ../train/mfcc/'+a+'/123.mfc\n')
  f.write('../train/wav/'+a+'/128.wav ../train/mfcc/'+a+'/128.mfc\n')
  f.write('../train/wav/'+a+'/133.wav ../train/mfcc/'+a+'/133.mfc\n')
  f.write('../train/wav/'+a+'/134.wav ../train/mfcc/'+a+'/134.mfc\n')
  f.write('../train/wav/'+a+'/140.wav ../train/mfcc/'+a+'/140.mfc\n')
  f.write('../train/wav/'+a+'/141.wav ../train/mfcc/'+a+'/141.mfc\n')
  f.write('../train/wav/'+a+'/146.wav ../train/mfcc/'+a+'/146.mfc\n')
  f.write('../train/wav/'+a+'/148.wav ../train/mfcc/'+a+'/148.mfc\n')
  f.write('../train/wav/'+a+'/158.wav ../train/mfcc/'+a+'/158.mfc\n')
  f.write('../train/wav/'+a+'/165.wav ../train/mfcc/'+a+'/165.mfc\n')
  f1.write('../train/mfcc/'+a+'/09.mfc\n')
  f1.write('../train/mfcc/'+a+'/11.mfc\n')
  f1.write('../train/mfcc/'+a+'/24.mfc\n')
  f1.write('../train/mfcc/'+a+'/37.mfc\n')
  f1.write('../train/mfcc/'+a+'/38.mfc\n')
  f1.write('../train/mfcc/'+a+'/39.mfc\n')
  f1.write('../train/mfcc/'+a+'/41.mfc\n')
  f1.write('../train/mfcc/'+a+'/42.mfc\n')
  f1.write('../train/mfcc/'+a+'/43.mfc\n')
  f1.write('../train/mfcc/'+a+'/44.mfc\n')
  f1.write('../train/mfcc/'+a+'/45.mfc\n')
  f1.write('../train/mfcc/'+a+'/47.mfc\n')
  f1.write('../train/mfcc/'+a+'/53.mfc\n')
  f1.write('../train/mfcc/'+a+'/54.mfc\n')
  f1.write('../train/mfcc/'+a+'/55.mfc\n')
  f1.write('../train/mfcc/'+a+'/56.mfc\n')
  f1.write('../train/mfcc/'+a+'/57.mfc\n')
  f1.write('../train/mfcc/'+a+'/59.mfc\n')
  f1.write('../train/mfcc/'+a+'/60.mfc\n')
  f1.write('../train/mfcc/'+a+'/62.mfc\n')
  f1.write('../train/mfcc/'+a+'/68.mfc\n')
  f1.write('../train/mfcc/'+a+'/69.mfc\n')
  f1.write('../train/mfcc/'+a+'/72.mfc\n')
  f1.write('../train/mfcc/'+a+'/74.mfc\n')
  f1.write('../train/mfcc/'+a+'/75.mfc\n')
  f1.write('../train/mfcc/'+a+'/81.mfc\n')
  f1.write('../train/mfcc/'+a+'/83.mfc\n')
  f1.write('../train/mfcc/'+a+'/84.mfc\n')
  f1.write('../train/mfcc/'+a+'/90.mfc\n')
  f1.write('../train/mfcc/'+a+'/95.mfc\n')
  f1.write('../train/mfcc/'+a+'/99.mfc\n')
  f1.write('../train/mfcc/'+a+'/118.mfc\n')
  f1.write('../train/mfcc/'+a+'/119.mfc\n')
  f1.write('../train/mfcc/'+a+'/123.mfc\n')
  f1.write('../train/mfcc/'+a+'/128.mfc\n')
  f1.write('../train/mfcc/'+a+'/133.mfc\n')
  f1.write('../train/mfcc/'+a+'/134.mfc\n')
  f1.write('../train/mfcc/'+a+'/140.mfc\n')
  f1.write('../train/mfcc/'+a+'/141.mfc\n')
  f1.write('../train/mfcc/'+a+'/146.mfc\n')
  f1.write('../train/mfcc/'+a+'/148.mfc\n')
  f1.write('../train/mfcc/'+a+'/158.mfc\n')
  f1.write('../train/mfcc/'+a+'/165.mfc\n')
  for i in range(176, 198):
    if i != 188 and i != 191 and i != 192:
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')
  for i in range(202, 233):
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')
  for i in range(251, 283):
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')
f.close()
f1.close()
