#! /bin/bash
# coding: utf-8
####################################################################
###
### archivo: entrenamientoJulius.sh
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

# generamos los archivos dict, term, dfa
#mkdfa.pl ordenes

echo "Creamos una lista de palabras"
#perl ../HTK_scripts/prompts2wlist prompts.txt wlist
../julia/bin/julia ../bin/prompts2wlist.jl prompts.txt wList

#echo "Agregando SENT-END y SENT-START a wlist"
#echo SENT-END > wlistTemp
#echo SENT-START >> wlistTemp
#cat wlist >> wlistTemp
#rm wlist
#mv wlistTemp wlist

echo "Creando monophones1"
HDMan -A -D -T 1 -m -w wList -n monophones1 -i -l dlog dict ../lexicon/VoxForgeDict.txt

echo "Creando monophones0 copiando a monophones1 y eliminando 'sp'"
cat monophones1 | grep -v sp >monophones0
#cp monophones1 monophones0

echo "Paso 3 - Creando words.mlf"
#perl ../HTK_scripts/prompts2mlf words.mlf prompts.txt
../julia/bin/julia ../bin/prompts2mlf.jl prompts.txt words.mlf
HLEd -A -D -T 1 -l '*' -d dict -i phones0.mlf ../input_files/mkphones0.led words.mlf

echo "Paso 4"
HLEd -A -D -T 1 -l '*' -d dict -i phones1.mlf ../input_files/mkphones1.led words.mlf

echo "Paso 5 - convirtiendo de wav a mfc"
HCopy -A -D -T 1 -C ../input_files/wav_config -S codetrain.scp

echo "Paso 6 - eliminando las carpetas hmm viejas"
rm -Rf hmm[0-9] hmm1[0-5]

echo "Creando las carpetas hmm0...hmm15"
mkdir hmm0 hmm1 hmm2 hmm3 hmm4 hmm5 hmm6 hmm7 hmm8 hmm9 hmm10 hmm11 hmm12 hmm13 hmm14 hmm15

HCompV -A -D -T 1 -C ../input_files/config -f 0.01 -m -S train.scp -M hmm0 ../input_files/proto

echo "Creando en hmm0: hmdefs y macros"
../script/./hmmdefs.py
../script/./macros.py

echo "Restimando los modelos hmmdefs y macros en hmm1, hmm2 y hmm3"
HERest -A -D -T 1 -C ../input_files/config -I phones0.mlf -t 250.0 150.0 1000.0 -S train.scp -H hmm0/macros -H hmm0/hmmdefs -M hmm1 monophones0
HERest -A -D -T 1 -C ../input_files/config -I phones0.mlf -t 250.0 150.0 1000.0 -S train.scp -H hmm1/macros -H hmm1/hmmdefs -M hmm2 monophones0
HERest -A -D -T 1 -C ../input_files/config -I phones0.mlf -t 250.0 150.0 1000.0 -S train.scp -H hmm2/macros -H hmm2/hmmdefs -M hmm3 monophones0

echo "Paso 7- copiando hmm3 a hmm4"
cp hmm3/* hmm4/

echo "Paso 8 - Agregando estado 'sp'"
../script/./sp.py

echo "Paso 9- hmm5"
HHEd -A -D -T 1 -H hmm4/macros -H hmm4/hmmdefs -M hmm5 ../input_files/sil.hed monophones1

echo"Paso 10 - hmm6 y hmm7"
HERest -A -D -T 1 -C ../input_files/config  -I phones1.mlf -t 250.0 150.0 3000.0 -S train.scp -H hmm5/macros -H  hmm5/hmmdefs -M hmm6 monophones1
HERest -A -D -T 1 -C ../input_files/config  -I phones1.mlf -t 250.0 150.0 3000.0 -S train.scp -H hmm6/macros -H  hmm6/hmmdefs -M hmm7 monophones1

echo "Paso 11 - Alineación"
HVite -A -D -T 1 -l '*' -o SWT -b SENT-END -C ../input_files/config -H hmm7/macros -H hmm7/hmmdefs -i aligned.mlf -m -t 250.0 150.0 1000.0 -y lab -a -I words.mlf -S train.scp dict monophones1> hvite_log

echo "Paso 12-  hmm8 y hmm9"
HERest -A -D -T 1 -C ../input_files/config -I aligned.mlf -t 250.0 150.0 3000.0 -S train.scp -H hmm7/macros -H hmm7/hmmdefs -M hmm8 monophones1
HERest -A -D -T 1 -C ../input_files/config -I aligned.mlf -t 250.0 150.0 3000.0 -S train.scp -H hmm8/macros -H hmm8/hmmdefs -M hmm9 monophones1

echo "Paso 13"
HLEd -A -D -T 1 -n triphones1 -l '*' -i wintri.mlf ../input_files/mktri.led aligned.mlf

echo "Paso 14: creando el archivo 'mktri.hed'"
../julia/bin/julia ../bin/mktrihed.jl monophones1 triphones1 mktri.hed

echo "creamos hmdfes y macros para la carpeta hmm10"
HHEd -A -D -T 1 -H hmm9/macros -H hmm9/hmmdefs -M hmm10 mktri.hed monophones1

echo "Para hmm11 y hmm12"
HERest  -A -D -T 1 -C ../input_files/config -I wintri.mlf -t 250.0 150.0 3000.0 -s stats -S train.scp -H hmm10/macros -H hmm10/hmmdefs -M hmm11 triphones1
HERest  -A -D -T 1 -C ../input_files/config -I wintri.mlf -t 250.0 150.0 3000.0 -s stats -S train.scp -H hmm11/macros -H hmm11/hmmdefs -M hmm12 triphones1

echo "Paso 15- triphones"
HDMan -A -D -T 1 -b sp -n fulllist0 -g ../input_files/maketriphones.ded -l flog dict-tri ../lexicon/VoxForgeDict.txt
../julia/bin/julia ../bin/fixfulllist.jl fulllist0 monophones0 fulllist

echo  "copiamos tree1.hed a tree.hed"
cat ../input_files/tree1.hed > tree.hed
#perl ../HTK_scripts/mkclscript.prl TB 350 monophones0 >> tree.hed
#cat tree >> tree.hed
../julia/bin/julia ../bin/mkclscript.jl monophones0 tree.hed

echo "para hmm13"
HHEd -A -D -T 1 -H hmm12/macros -H hmm12/hmmdefs -M hmm13 tree.hed triphones1 

echo "para hmm14 y hmm15"
HERest -A -D -T 1 -T 1 -C ../input_files/config -I wintri.mlf  -t 250.0 150.0 3000.0 -S train.scp -H hmm13/macros -H hmm13/hmmdefs -M hmm14 tiedlist
HERest -A -D -T 1 -T 1 -C ../input_files/config -I wintri.mlf  -t 250.0 150.0 3000.0 -S train.scp -H hmm14/macros -H hmm14/hmmdefs -M hmm15 tiedlist

echo "Listo, puedes usar 'hmm15/hmmdes' y 'tiedlist' para hacer reconocimiento"
echo "Solo ejecuta:
julius -input mic -C julian.jconf"
