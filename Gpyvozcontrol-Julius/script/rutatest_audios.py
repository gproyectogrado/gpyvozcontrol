#! /usr/bin/env python
# coding: utf-8

import os

data = '../train/wav/'
data1 = '../train/mfcc/'
carpeta = [
  'prueba1',
  'prueba2',
  'prueba3'
]
f = open('codetraintest.scp', 'a')
f1 = open('traintest.scp', 'a')

t=52
for a in carpeta:
  if a == 'prueba2':
    t=101
  for i in range(1, t):
    if i <= 9:
      f.write(data+a+'/0'+str(i)+'.wav '+data1+a+'/0'+str(i)+'.mfc\n')
      f1.write(data1+a+'/0'+str(i)+'.mfc\n')
    else:
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')
  t=52
f.close()
f1.close()
