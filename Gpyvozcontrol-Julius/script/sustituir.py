#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: sustituir.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import re

archivo = ['usuario']
lineas=' '
for texto in archivo:
    f = open(texto, 'r')
    data = f.read()
    lineas = data.splitlines()
    f.close()

separado=""
actor=open('actor', 'w')
usuario=""
for a in lineas:
  usuario=a
  for b in a:
    separado += b+' '

separado = re.sub('h', '', separado)
separado = re.sub('c c', 'k th', separado)
separado = re.sub('x a', 'k s a', separado)
separado = re.sub('x e', 'k s e', separado)
separado = re.sub('x i', 'k s i', separado)
separado = re.sub('x o', 'k s o', separado)
separado = re.sub('x u', 'k s u', separado)
separado = re.sub('v', 'b', separado)
separado = re.sub('c a', 'k a', separado)
separado = re.sub('c o', 'k o', separado)
separado = re.sub('c u', 'k u', separado)
separado = re.sub('c e', 'th e', separado)
separado = re.sub('c i', 'th i', separado)
separado = re.sub('j', 'x', separado)
separado = re.sub('y a', 'll a', separado)
separado = re.sub('y e', 'll e', separado)
separado = re.sub('y i', 'll i', separado)
separado = re.sub('y o', 'll o', separado)
separado = re.sub('y u', 'll u', separado)
separado = re.sub('y', 'i', separado)
separado = re.sub('ñ', 'ny', separado)
separado = re.sub('q u', 'k', separado)
separado = re.sub('z', 'th', separado)
separado = re.sub('w', 'u', separado)
separado = re.sub('g e', 'x e', separado)
separado = re.sub('g i', 'x i', separado)
separado = re.sub('c', 'k', separado)
separado = re.sub('r r', 'rr', separado)

text=usuario+' '+separado
print text
actor.write(text)
actor.close()
