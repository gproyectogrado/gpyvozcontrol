#! /usr/bin/env python
# coding: utf-8

import os

data = '../train/wav/'
data1 = '../train/mfcc/'
carpeta = [
  #'buhochileno4',
  #'dpinto1',
  #'fvera1',
  #'jalvarado1',
  #'mmarin1',
  #'msolis1',
  #'ppalma1',
  #'txita1',
  #'ubanov1',
  #'ubanov2',
  'quinteron',
  'guzmanl',
  'ramirezm',
  'coronam',
  'montillaf',
  'garciaj',
  'moym',
  'rincony',
  'rodrigueza'
]
f = open('codetrain.scp', 'w')
f1 = open('train.scp', 'w')

for a in carpeta:
  for i in range(1, 201):
    if i <= 9:
      f.write(data+a+'/0'+str(i)+'.wav '+data1+a+'/0'+str(i)+'.mfc\n')
      f1.write(data1+a+'/0'+str(i)+'.mfc\n')
    else:
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')

f.close()
f1.close()
