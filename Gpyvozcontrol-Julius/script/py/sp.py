#! /usr/bin/env python
# coding: utf-8

import os

file = open('hmm4/hmmdefs', 'r')
file1 = open('hmm4/hmmdefsTemp', 'w')
sp = ""
sil = '~h "sil"'
n=0
for text in file:
  file1.write(text)
  if text == '~h "sil"\n':
    n=1
  if n==2 and text == "<TRANSP> 5\n":
    n=1
  if n==2:
    sp += text
  if n==1 and text == "<STATE> 4\n":
    n = 2

file1.write('~h "sp"\n')
file1.write('<BEGINHMM>\n')
file1.write('<NUMSTATES> 3\n')
file1.write('<STATE> 2\n')
file1.write(sp)
file1.write('<TRANSP> 3\n')
file1.write(' 0.0 1.0 0.0\n 0.0 0.9 0.1\n 0.0 0.0 0.0\n<ENDHMM>')
file.close()
file1.close()
os.system('rm hmm4/hmmdefs;mv hmm4/hmmdefsTemp hmm4/hmmdefs')
