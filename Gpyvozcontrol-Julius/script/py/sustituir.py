#! /usr/bin/env python
# coding: utf-8

import re

archivo = ['usuario']
lineas=' '
for texto in archivo:
    f = open(texto, 'r')
    data = f.read()
    lineas = data.splitlines()
    f.close()

separado=""
actor=open('actor', 'w')
usuario=""
for a in lineas:
  usuario=a
  for b in a:
    separado += b+' '

separado = re.sub('h', '', separado)
separado = re.sub('c c', 'k th', separado)
separado = re.sub('x a', 'k s a', separado)
separado = re.sub('x e', 'k s e', separado)
separado = re.sub('x i', 'k s i', separado)
separado = re.sub('x o', 'k s o', separado)
separado = re.sub('x u', 'k s u', separado)
separado = re.sub('v', 'b', separado)
separado = re.sub('c a', 'k a', separado)
separado = re.sub('c o', 'k o', separado)
separado = re.sub('c u', 'k u', separado)
separado = re.sub('c e', 'th e', separado)
separado = re.sub('c i', 'th i', separado)
separado = re.sub('j', 'x', separado)
separado = re.sub('y a', 'll a', separado)
separado = re.sub('y e', 'll e', separado)
separado = re.sub('y i', 'll i', separado)
separado = re.sub('y o', 'll o', separado)
separado = re.sub('y u', 'll u', separado)
separado = re.sub('y', 'i', separado)
separado = re.sub('ñ', 'ny', separado)
separado = re.sub('q u', 'k', separado)
separado = re.sub('z', 'th', separado)
separado = re.sub('w', 'u', separado)
separado = re.sub('g e', 'x e', separado)
separado = re.sub('g i', 'x i', separado)
separado = re.sub('c', 'k', separado)
separado = re.sub('r r', 'rr', separado)

text=usuario+' '+separado
print text
actor.write(text)
actor.close()
