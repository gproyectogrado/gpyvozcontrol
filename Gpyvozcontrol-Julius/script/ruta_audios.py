#! /usr/bin/env python
# coding: utf-8
####################################################################
###
### archivo: ruta_audios.py
### version: 0.1
### autor: Jorge Ortega
### Correo: libretecnologia@gmail.com
### tutor: Jacinto Dávila
### Correo: jacinto.davila@gmail.com
### Fecha: Diciembre - 2015
###
### Copyright (C) 2015 Jorge Ortega
###
### Este programa es software libre; puedes redistribuirlo y / o
### Modificarlo bajo los términos de la Licencia Pública General GNU Affero
### Publicada por la Fundación para el Software Libre; ya sea la versión 3.0
### De la Licencia, o cualquier versión posterior.
###
### Este programa se distribuye con la esperanza de que sea útil,
### Pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
### COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Revisar
### la Licencia Publica General GNU Affero para mayor información.
###                                                                 
### Debería haber recibido una copia de la Licencia Pública General GNU Affero junto con este programa.
### Si no es así, consulte <http://www.gnu.org/licenses/>.
####################################################################

import os

data = '../train/wav/'
data1 = '../train/mfcc/'
carpeta = [
  #'quinteron',
  #'guzmanl',
  #'ramirezm',
  #'coronam',
  #'montillaf',
  #'garciaj',
  #'moym',
  #'rincony',
  #'rodrigueza',
  #'cadavidd',
  #'buelam',
  #'vanessa',
  #'peñay',
  #'santiagol',
  'molinaj',
]
f = open('codetrain.scp', 'w')
f1 = open('train.scp', 'w')

for a in carpeta:
  os.system('mkdir ../train/mfcc/'+a)
  for i in range(1, 201):
    if i <= 9:
      f.write(data+a+'/0'+str(i)+'.wav '+data1+a+'/0'+str(i)+'.mfc\n')
      f1.write(data1+a+'/0'+str(i)+'.mfc\n')
    else:
      f.write(data+a+'/'+str(i)+'.wav '+data1+a+'/'+str(i)+'.mfc\n')
      f1.write(data1+a+'/'+str(i)+'.mfc\n')

f.close()
f1.close()
